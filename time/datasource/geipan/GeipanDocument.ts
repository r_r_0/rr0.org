import { GeipanDocumentType } from "./GeipanDocumentType"

export type GeipanDocument = {
  type: GeipanDocumentType
  contents: any
}
