import { City } from "../../../country/region/department/city/City"
import { dunmore } from "./dunmore/Dunmore"

export const newSouthWalesCities: City[] = [
  dunmore
]
