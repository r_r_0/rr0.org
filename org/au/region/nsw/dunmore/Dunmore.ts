import { NewSouthWalesCityCode } from "../NewSouthWalesCityCode"
import { Place } from "../../../../../place/Place"
import { newSouthWales } from "../NewSouthWales"
import { City } from "../../../../country/region/department/city/City"

export const dunmore = new City(NewSouthWalesCityCode.Dunmore, newSouthWales, [Place.fromLocation(-34.62, 150.83)])
