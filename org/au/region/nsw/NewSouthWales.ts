import { australiaRegion } from "../AustraliaRegion"
import { AustraliaRegionCode } from "../AustraliaRegionCode"
import { Place } from "../../../../place/Place"

export let newSouthWales = australiaRegion(AustraliaRegionCode.nsw, Place.fromDMS("32°0'S,147°0'E"))
