import { australiaRegion } from "../AustraliaRegion"
import { AustraliaRegionCode } from "../AustraliaRegionCode"
import { Place } from "../../../../place/Place"

export let victoria = australiaRegion(AustraliaRegionCode.vic, Place.fromLocation(47.466667, 0.833333))
