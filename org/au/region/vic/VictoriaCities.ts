import { City } from "../../../country/region/department/city/City"
import { melbourneVic } from "./melbourne/Melbourne"

export const victoriaCities: City[] = [
  melbourneVic
]
