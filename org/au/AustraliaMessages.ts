import { RegionMessages } from "../country/region/RegionMessages"
import { AustraliaRegionCode } from "./region/AustraliaRegionCode"

export type AustraliaMessages = { [key in AustraliaRegionCode]: RegionMessages }
