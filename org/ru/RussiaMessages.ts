import { RegionMessages } from "../country/region/RegionMessages"
import { RussiaRegionCode } from "./region/RussiaRegionCode"

export type RussiaRegionMessagesList = { [key in RussiaRegionCode]: RegionMessages }
