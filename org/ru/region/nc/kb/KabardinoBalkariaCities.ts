import { mountElbrus } from "./elbrus/MountElbrus"
import { City } from "../../../../country/region/department/city/City"

export const kabardinoBalkariaCities: City[] = [
  mountElbrus
]
