import { KabardinoBalkariaCityCode } from "./KabardinoBalkariaCityCode"
import { CityMessages } from "../../../../country/region/department/city/CityMessages"

export type KabardinoBalkariaCityMessagesList = { [key in KabardinoBalkariaCityCode]: CityMessages }
