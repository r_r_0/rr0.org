import { RegionMessages } from "../../../country/region/RegionMessages"
import { NorthCaucasusDepartementCode } from "./NorthCaucasusDepartementCode"
import { kabardinoBalkaria_fr } from "./kb/KabardinoBalkaria_fr"

export const northCaucasus_fr = RegionMessages.create("Caucase du Nord", {
  [NorthCaucasusDepartementCode.SusseKabardinoBalkaria]: kabardinoBalkaria_fr
})
