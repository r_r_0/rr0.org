import { RussiaDepartementCode } from "../RussiaDepartementCode"

export enum NorthCaucasusDepartementCode {
  SusseKabardinoBalkaria = RussiaDepartementCode.KabardinoBalkaria
}
