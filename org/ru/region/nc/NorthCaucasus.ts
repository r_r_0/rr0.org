import { RussiaRegionCode } from "../RussiaRegionCode"
import { Place } from "../../../../place/Place"
import { russiaRegion } from "../../Russia"

export const northCaucasus = russiaRegion(RussiaRegionCode.nc, Place.fromDMS("53°0'N,1°0'W"))
