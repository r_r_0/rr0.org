import { City } from "../../../country/region/department/city/City"
import { kabardinoBalkariaCities } from "./kb/KabardinoBalkariaCities"

export const northCaucasusCities: City[] = [
  ...kabardinoBalkariaCities
]
