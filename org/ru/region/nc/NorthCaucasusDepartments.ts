import { Department } from "../../../country/region/department/Department"
import { kabardinoBalkaria } from "./kb/KabardinoBalkaria"

export const northCaucasusDepartments: Department[] = [
  kabardinoBalkaria
]
