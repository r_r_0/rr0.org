import { RegionMessages } from "../../../country/region/RegionMessages"
import { NorthCaucasusDepartementCode } from "./NorthCaucasusDepartementCode"
import { kabardinoBalkaria_en } from "./kb/KabardinoBalkaria_en"

export const northCaucasus_en = RegionMessages.create("North Caucasus", {
  [NorthCaucasusDepartementCode.SusseKabardinoBalkaria]: kabardinoBalkaria_en
})
