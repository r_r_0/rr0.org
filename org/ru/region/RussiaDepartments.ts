import { Department } from "../../country/region/department/Department"
import { northCaucasusDepartments } from "./nc/NorthCaucasusDepartments"

export const russiaDepartments: Department[] = [
  ...northCaucasusDepartments
]
