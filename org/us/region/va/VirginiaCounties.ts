import { Department } from "../../../country/region/department/Department"
import { arlington } from "./arlington/Arlington"

export const virginiaCounties: Department[] = [
  arlington
]
