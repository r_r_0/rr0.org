import { fortWorthMessages } from "./fortworth/FortWorthMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export let tarrant_fr = DepartmentMessages.create("Comté de Tarrant", {
    76133: fortWorthMessages
  }
)
