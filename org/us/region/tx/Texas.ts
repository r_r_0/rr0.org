import { Place } from "place/Place"
import { UsaStates } from "../UsaStates"
import { usaRegion } from "../../Usa"

export const texas = usaRegion(UsaStates.tx, Place.fromDMS("32°45′23″N 97°19′57″W"))
