import { tarrant } from "./tarrant/Tarrant"
import { Organization } from "../../../Organization"

export const texasCounties: Organization[] = [
  tarrant
]
