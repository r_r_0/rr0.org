import { UsaCountyCode } from "../UsaCountyCode"

export enum NebraskaCountyCode {
  Scottsbluff = UsaCountyCode.scottsBluff
}
