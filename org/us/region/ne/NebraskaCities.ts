import { City } from "../../../country/region/department/city/City"
import { scottsBluffCities } from "./ScottsBluff/ScottsBluffCities"

export const nebraskaCities: City[] = [
  ...scottsBluffCities
]
