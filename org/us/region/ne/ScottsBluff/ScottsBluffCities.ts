import { City } from "../../../../country/region/department/city/City"
import { scottsbluffCity } from "./Scottsbluff/ScottsbluffCity"

export const scottsBluffCities: City[] = [
  scottsbluffCity
]
