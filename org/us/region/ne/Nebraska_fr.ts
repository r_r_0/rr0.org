import { RegionMessages } from "../../../country/region/RegionMessages"
import { scottsBluff_fr } from "./ScottsBluff/ScottsBluff_fr"
import { NebraskaCountyCode } from "./NebraskaCountyCode"

export const nebraska_fr = RegionMessages.create("Nebraska", {
    [NebraskaCountyCode.Scottsbluff]: scottsBluff_fr
  }
)
