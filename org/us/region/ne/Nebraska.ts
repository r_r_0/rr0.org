import { usaRegion } from "../../Usa"
import { UsaStates } from "../UsaStates"
import { Place } from "../../../../place/Place"

export let nebraska = usaRegion(UsaStates.ne, Place.fromDMS("35°30′N,80°00′W"))
