import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const alabama_en = RegionMessages.create(
  "Alabama",
  {
    geneva: genevaMessages_en
  }
)
