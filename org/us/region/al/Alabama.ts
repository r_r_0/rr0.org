import { Place } from "place/Place"
import { UsaStates } from "../UsaStates"
import { usaRegion } from "../../Usa"

export const alabama = usaRegion(UsaStates.al, Place.fromLocation(47.466667, 0.833333))
