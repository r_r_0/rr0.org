import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const alabama_fr = RegionMessages.create("Alabama", {
  geneva: genevaMessages_fr
})
