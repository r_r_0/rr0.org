import { Department } from "../../../country/region/department/Department"
import { honolulu } from "./honolulu/Honolulu"

export const hawaiiCounties: Department[] = [
  honolulu
]
