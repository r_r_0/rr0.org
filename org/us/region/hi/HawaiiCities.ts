import { City } from "../../../country/region/department/city/City"
import { honoluluCities } from "./honolulu/HonoluluCities"

export const hawaiiCities: City[] = [
  ...honoluluCities
]
