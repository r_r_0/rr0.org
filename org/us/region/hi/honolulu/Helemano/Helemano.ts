import { Place } from "../../../../../../place/Place"
import { HonoluluCityCode } from "../HonoluluCityCode"
import { honolulu } from "../Honolulu"
import { usaCity } from "../../../UsaCity"

export let helemano = usaCity(HonoluluCityCode.Helemano, honolulu, Place.fromDMS("21°32′9″N 158°1′11″W"))
