import { Place } from "../../../../../../place/Place"
import { FayetteCityCode } from "../FayetteCityCode"
import { fayette } from "../Fayette"

import { usaCity } from "../../../UsaCity"

export let mountHope = usaCity(FayetteCityCode.MountHope, fayette, Place.fromDMS("37°53′33″N,81°10′4″W"))
