import { RegionMessages } from "../../../country/region/RegionMessages"
import { fayette_fr } from "./fayette/Fayette_fr"
import { mason_fr } from "./mason/Mason_fr"

export const westVirginia_fr = RegionMessages.create("Virginie-Occidentale", {
  fayette: fayette_fr,
  mason: mason_fr
})
