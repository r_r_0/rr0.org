import { fayette_en } from "./fayette/Fayette_en"
import { RegionMessages } from "../../../country/region/RegionMessages"
import { mason_en } from "./mason/Mason_en"

export const westVirginia_en = RegionMessages.create("West Virginia", {
  fayette: fayette_en,
  mason: mason_en
})
