import { Department } from "../../../country/region/department/Department"
import { fayette } from "./fayette/Fayette"
import { mason } from "./mason/Mason"

export const westVirginaCounties: Department[] = [
  fayette,
  mason
]
