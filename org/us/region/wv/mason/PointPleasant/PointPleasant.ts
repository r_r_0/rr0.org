import { Place } from "../../../../../../place/Place"
import { MasonCityCode } from "../MasonCityCode"
import { mason } from "../Mason"
import { usaCity } from "../../../UsaCity"

export let pointPleasant = usaCity(MasonCityCode.PointPleasant, mason, Place.fromDMS("40°04′59″N,74°04′05″W"))
