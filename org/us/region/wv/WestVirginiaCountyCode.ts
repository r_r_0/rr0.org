import { UsaCountyCode } from "../UsaCountyCode"

export enum WestVirginiaCountyCode {
  fayette = UsaCountyCode.fayette,
  mason = UsaCountyCode.mason,
}
