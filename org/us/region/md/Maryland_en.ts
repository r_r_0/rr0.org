import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const maryland_en = RegionMessages.create("Maryland", {
    geneva: genevaMessages_en
  }
)
