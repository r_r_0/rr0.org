import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const maryland_fr = RegionMessages.create("Maryland", {
  geneva: genevaMessages_fr
})
