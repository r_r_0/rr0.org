import { dane_fr } from "./dane/Dane_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const wisconsin_fr = RegionMessages.create("Wisconsin", {
  dane: dane_fr
})
