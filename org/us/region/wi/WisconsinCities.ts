import { daneCities } from "./dane/DaneCities"
import { City } from "../../../country/region/department/city/City"

export const wisconsinCities: City[] = [
  ...daneCities
]
