import { dane_en } from "./dane/Dane_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const wisconsin_en = RegionMessages.create("Wisconsin", {
  dane: dane_en
})
