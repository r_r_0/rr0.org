import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const nevada_fr = RegionMessages.create("Nevada", {
  kalamazoo: kalamazooMessages_fr
})
