import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const nevada_en = new RegionMessages(["Nevada"], {
  kalamazoo: kalamazooMessages_en
})
