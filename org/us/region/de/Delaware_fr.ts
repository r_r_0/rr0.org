import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const delaware_fr = RegionMessages.create("Delaware", {
  geneva: genevaMessages_fr
})
