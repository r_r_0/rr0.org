import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const delaware_en = RegionMessages.create("Delaware", {
    geneva: genevaMessages_en
  }
)
