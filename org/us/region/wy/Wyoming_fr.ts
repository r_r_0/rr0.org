import { carbon_fr } from "./carbon/Carbon_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const wyoming_fr = RegionMessages.create("Wyoming", {
  carbon: carbon_fr
})
