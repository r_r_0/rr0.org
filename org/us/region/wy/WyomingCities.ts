import { carbonCities } from "./carbon/CarbonCities"
import { City } from "../../../country/region/department/city/City"

export const wyomingCities: City[] = [
  ...carbonCities
]
