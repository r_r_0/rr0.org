import { carbon_en } from "./carbon/Carbon_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const wyoming_en = RegionMessages.create("Wyoming", {
  carbon: carbon_en
})
