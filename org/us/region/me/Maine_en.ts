import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const maine_en = RegionMessages.create("Maine", {
    geneva: genevaMessages_en
  }
)
