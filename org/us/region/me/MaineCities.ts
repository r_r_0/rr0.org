import { genevaCities } from "./geneva/GenevaCities"
import { City } from "../../../country/region/department/city/City"

export const maineCities: City[] = [
  ...genevaCities
]
