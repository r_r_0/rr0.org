import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const maine_fr = RegionMessages.create("Maine", {
  geneva: genevaMessages_fr
})
