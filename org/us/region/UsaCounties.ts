import { alabamaCounties } from "./al/AlabamaCounties"
import { californiaCounties } from "./ca/CaliforniaCounties"
import { floridaCounties } from "./fl/FloridaCounties"
import { texasCounties } from "./tx/TexasCounties"
import { pennsylvaniaCounties } from "./pa/PennsylvaniaCounties"
import { washingtonCounties } from "./wa/WashingtonCounties"
import { Organization } from "../../Organization"
import { newMexicoCounties } from "./nm/NewMexicoCounties"
import { newJerseyCounties } from "./nj/NewJerseyCounties"
import { newYorkCounties } from "./ny/NewYorkCounties"
import { westVirginaCounties } from "./wv/WestVirginiaCounties"
import { virginiaCounties } from "./va/VirginiaCounties"
import { hawaiiCounties } from "./hi/HawaiiCounties"
import { nebraskaCounties } from "./ne/NebraskaCounties"
import { arkansasCounties } from "./ak/ArkansasCounties"
import { connecticutCounties } from "./ct/ConnecticutCounties"
import { coloradoCounties } from "./co/ColoradoCounties"
import { delawareCounties } from "./de/DelawareCounties"
import { illinoisCounties } from "./il/IllinoisCounties"
import { kentuckyCounties } from "./ky/KentuckyCounties"
import { maineCounties } from "./me/MaineCounties"
import { marylandCounties } from "./md/MarylandCounties"
import { massachusettsCounties } from "./ma/MassachusettsCounties"
import { michiganCounties } from "./mi/MichiganCounties"
import { minnesotaCounties } from "./mn/MinnesotaCounties"
import { mississippiCounties } from "./ms/MississippiCounties"
import { missouriCounties } from "./mo/MissouriCounties"
import { montanaCounties } from "./mt/MontanaCounties"
import { newHampshireCounties } from "./nh/NewHampshireCounties"
import { nevadaCounties } from "./nv/NevadaCounties"
import { ohioCounties } from "./oh/OhioCounties"
import { oklahomaCounties } from "./ok/OklahomaCounties"
import { oregonCounties } from "./or/OregonCounties"
import { puertoRicoCounties } from "./pr/PuertoRicoCounties"
import { southCarolinaCities } from "./sc/SouthCarolinaCities"
import { utahCounties } from "./ut/UtahCounties"
import { vermontCounties } from "./vt/VermontCounties"
import { virginIslandsCounties } from "./vi/VirginIslandsCounties"
import { wisconsinCounties } from "./wi/WisconsinCounties"
import { wyomingCounties } from "./wy/WyomingCounties"

export const usaCounties: Organization[] = [
  ...alabamaCounties,
  ...arkansasCounties,
  ...californiaCounties,
  ...coloradoCounties,
  ...connecticutCounties,
  ...delawareCounties,
  ...floridaCounties,
  ...hawaiiCounties,
  ...illinoisCounties,
  ...kentuckyCounties,
  ...maineCounties,
  ...marylandCounties,
  ...massachusettsCounties,
  ...michiganCounties,
  ...minnesotaCounties,
  ...mississippiCounties,
  ...missouriCounties,
  ...montanaCounties,
  ...nevadaCounties,
  ...newHampshireCounties,
  ...nebraskaCounties,
  ...newJerseyCounties,
  ...newMexicoCounties,
  ...newYorkCounties,
  ...ohioCounties,
  ...oklahomaCounties,
  ...oregonCounties,
  ...pennsylvaniaCounties,
  ...puertoRicoCounties,
  ...southCarolinaCities,
  ...texasCounties,
  ...utahCounties,
  ...vermontCounties,
  ...virginiaCounties,
  ...virginIslandsCounties,
  ...washingtonCounties,
  ...westVirginaCounties,
  ...wisconsinCounties,
  ...wyomingCounties
]
