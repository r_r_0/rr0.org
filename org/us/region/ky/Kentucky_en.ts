import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const kentucky_en = RegionMessages.create("Kentucky", {
    geneva: genevaMessages_en
  }
)
