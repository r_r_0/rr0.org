import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const kentucky_fr = RegionMessages.create("Kentucky", {
  geneva: genevaMessages_fr
})
