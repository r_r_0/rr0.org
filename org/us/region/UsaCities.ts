import { alabamaCities } from "./al/AlabamaCities"
import { californiaCities } from "./ca/CaliforniaCities"
import { texasCities } from "./tx/TexasCities"
import { floridaCities } from "./fl/FloridaCities"
import { pennsylvaniaCities } from "./pa/PennsylvaniaCities"
import { washingtonCities } from "./wa/WashingtonCities"
import { indianaCities } from "./in/IndianaCities"
import { City } from "../../country/region/department/city/City"
import { northCarolinaCities } from "./nc/NorthCarolinaCities"
import { newJerseyCities } from "./nj/NewJerseyCities"
import { newYorkCities } from "./ny/NewYorkCities"
import { westVirginiaCities } from "./wv/WestVirginiaCities"
import { virginiaCities } from "./va/VirginiaCities"
import { hawaiiCities } from "./hi/HawaiiCities"
import { nebraskaCities } from "./ne/NebraskaCities"
import { arkansasCities } from "./ak/ArkansasCities"
import { connecticutCities } from "./ct/ConnecticutCities"
import { coloradoCities } from "./co/ColoradoCities"
import { delawareCities } from "./de/DelawareCities"
import { illinoisCities } from "./il/IllinoisCities"
import { kentuckyCities } from "./ky/KentuckyCities"
import { maineCities } from "./me/MaineCities"
import { marylandCities } from "./md/MarylandCities"
import { massachusettsCities } from "./ma/MassachusettsCities"
import { michiganCities } from "./mi/MichiganCities"
import { minnesotaCities } from "./mn/MinnesotaCities"
import { mississippiCities } from "./ms/MississippiCities"
import { missouriCities } from "./mo/MissouriCities"
import { montanaCities } from "./mt/MontanaCities"
import { newHampshireCities } from "./nh/NewHampshireCities"
import { nevadaCities } from "./nv/NevadaCities"
import { ohioCities } from "./oh/OhioCities"
import { oklahomaCities } from "./ok/OklahomaCities"
import { oregonCities } from "./or/OregonCities"
import { puertoRicoCities } from "./pr/PuertoRicoCities"
import { southCarolinaCities } from "./sc/SouthCarolinaCities"
import { utahCities } from "./ut/UtahCities"
import { vermontCities } from "./vt/VermontCities"
import { virginIslandsCities } from "./vi/VirginIslandsCities"
import { wisconsinCities } from "./wi/WisconsinCities"
import { wyomingCities } from "./wy/WyomingCities"

export const usaCities: City[] = [
  ...alabamaCities,
  ...arkansasCities,
  ...californiaCities,
  ...coloradoCities,
  ...connecticutCities,
  ...delawareCities,
  ...floridaCities,
  ...hawaiiCities,
  ...illinoisCities,
  ...indianaCities,
  ...kentuckyCities,
  ...maineCities,
  ...marylandCities,
  ...massachusettsCities,
  ...michiganCities,
  ...minnesotaCities,
  ...mississippiCities,
  ...missouriCities,
  ...montanaCities,
  ...nevadaCities,
  ...newHampshireCities,
  ...ohioCities,
  ...oklahomaCities,
  ...oregonCities,
  ...pennsylvaniaCities,
  ...puertoRicoCities,
  ...nebraskaCities,
  ...newJerseyCities,
  ...newYorkCities,
  ...northCarolinaCities,
  ...southCarolinaCities,
  ...texasCities,
  ...utahCities,
  ...vermontCities,
  ...virginiaCities,
  ...virginIslandsCities,
  ...washingtonCities,
  ...westVirginiaCities,
  ...wisconsinCities,
  ...wyomingCities
]
