import { pinellasCities } from "./pinellas/PinellasCities"
import { City } from "../../../country/region/department/city/City"

export const floridaCities: City[] = [
  ...pinellasCities
]
