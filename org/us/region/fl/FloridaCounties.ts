import { pinellas } from "./pinellas/Pinellas"
import { Organization } from "../../../Organization"

export const floridaCounties: Organization[] = [
  pinellas
]
