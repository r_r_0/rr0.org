import { stPetersburgMessages } from "./stpetersburg/StPetersburgMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export let pinellasMessages_fr = DepartmentMessages.create("Comté de Pinellas", {
    33701: stPetersburgMessages
  }
)
