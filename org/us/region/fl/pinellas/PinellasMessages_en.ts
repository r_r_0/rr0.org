import { stPetersburgMessages } from "./stpetersburg/StPetersburgMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export let pinellasMessages_en = DepartmentMessages.create(
  "Pinellas County",
  {
    33701: stPetersburgMessages
  }
)
