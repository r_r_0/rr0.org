import { RegionMessages } from "../../../country/region/RegionMessages"
import { pinellasMessages_en } from "./pinellas/PinellasMessages_en"

export const florida_en = RegionMessages.create(
  "Florida",
  {
    pinellas: pinellasMessages_en
  }
)
