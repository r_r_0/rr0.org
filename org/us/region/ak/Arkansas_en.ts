import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const arkansas_en = RegionMessages.create("Arkansas", {
    geneva: genevaMessages_en
  }
)
