import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const arkansas_fr = RegionMessages.create("Arkansas", {
  geneva: genevaMessages_fr
})
