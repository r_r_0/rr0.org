import { galesburg } from "./galesburg/Galesburg"
import { City } from "../../../../country/region/department/city/City"

export const kalamazooCities: City[] = [
  galesburg
]
