import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const missouri_en = new RegionMessages(["Missouri"], {
  kalamazoo: kalamazooMessages_en
})
