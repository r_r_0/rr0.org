import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const missouri_fr = RegionMessages.create("Missouri", {
  kalamazoo: kalamazooMessages_fr
})
