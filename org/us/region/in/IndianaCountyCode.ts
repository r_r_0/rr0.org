import { UsaCountyCode } from "../UsaCountyCode"

export enum IndianaCountyCode {
  elkhart = UsaCountyCode.elkhart,
  monroe = UsaCountyCode.monroe,
  whitley = UsaCountyCode.whitley,
}
