import { Place } from "../../../../../../place/Place"
import { MonroeCityCode } from "../MonroeCityCode"
import { monroe } from "../Monroe"

import { usaCity } from "../../../UsaCity"

export let bloomington = usaCity(MonroeCityCode.Bloomington, monroe, Place.fromDMS("39°09′44″N,86°31′45″O"))
