import { bloomington } from "./bloomington/Bloomington"
import { City } from "../../../../country/region/department/city/City"

export const monroeCities: City[] = [
  bloomington
]
