import { DepartmentMessages } from "../../../country/region/department/DepartmentMessages"
import { IndianaCountyCode } from "./IndianaCountyCode"

export type IndianaMessages = { [key in IndianaCountyCode]: DepartmentMessages }
