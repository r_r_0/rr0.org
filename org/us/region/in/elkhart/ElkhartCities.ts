import { elkhartCity } from "./elkhart/ElkhartCity"
import { City } from "../../../../country/region/department/city/City"

export const elkhartCities: City[] = [
  elkhartCity
]
