import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"
import { OrganizationMessages } from "../../../../OrganizationMessages"

export const chaves_en = DepartmentMessages.create("Chaves County", {
    88201: new OrganizationMessages("Roswell"),
    88202: new OrganizationMessages("Roswell"),
    88203: new OrganizationMessages("Roswell")
  }
)
