import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"
import { OrganizationMessages } from "../../../../OrganizationMessages"

export const chaves_fr = DepartmentMessages.create("Comté de Chaves", {
    88201: new OrganizationMessages("Roswell"),
    88202: new OrganizationMessages("Roswell"),
    88203: new OrganizationMessages("Roswell")
  }
)
