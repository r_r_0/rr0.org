import { RegionMessages } from "../../../country/region/RegionMessages"
import { chaves_en } from "./chaves/Chaves_en"

export const newMexico_en = RegionMessages.create("New Mexico", {
    chaves: chaves_en
  }
)
