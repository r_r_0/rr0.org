import { RegionMessages } from "../../../country/region/RegionMessages"
import { chaves_fr } from "./chaves/Chaves_fr"

export const newMexico_fr = RegionMessages.create("Nouveau-Mexique", {
    chaves: chaves_fr
  }
)
