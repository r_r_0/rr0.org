import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const southCarolina_en = new RegionMessages(["South Carolina"], {
  kalamazoo: kalamazooMessages_en
})
