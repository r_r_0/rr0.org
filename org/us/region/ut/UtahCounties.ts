import { uintah } from "./uintah/Uintah"
import { Organization } from "../../../Organization"

export const utahCounties: Organization[] = [
  uintah
]
