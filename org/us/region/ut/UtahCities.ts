import { uintahCities } from "./uintah/UintahCities"
import { City } from "../../../country/region/department/city/City"

export const utahCities: City[] = [
  ...uintahCities
]
