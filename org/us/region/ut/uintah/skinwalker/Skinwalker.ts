import { Place } from "../../../../../../place/Place"
import { uintah } from "../Uintah"
import { UintahCityCode } from "../UintahCityCode"
import { usaCity } from "../../../UsaCity"

export let skinwalker = usaCity(UintahCityCode.Skinwalker, uintah, Place.fromDMS("40°15′29″N 109°53′18″W"))
