import { uintah_fr } from "./uintah/Uintah_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const utah_fr = RegionMessages.create("Utah", {
  uintah: uintah_fr
})
