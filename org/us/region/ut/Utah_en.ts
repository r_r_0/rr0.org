import { uintah_en } from "./uintah/Uintah_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const utah_en = RegionMessages.create("Utah", {
  uintah: uintah_en
})
