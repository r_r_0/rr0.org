import { usaRegion } from "../../Usa"
import { UsaStates } from "../UsaStates"
import { Place } from "../../../../place/Place"

export let northCarolina = usaRegion(UsaStates.nc, Place.fromDMS("35°30′N,80°00′W"))
