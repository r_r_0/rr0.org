import { City } from "../../../country/region/department/city/City"
import { guilfordCities } from "./guilford/GuilfordCities"

export const northCarolinaCities: City[] = [
  ...guilfordCities
]
