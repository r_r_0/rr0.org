import { Place } from "../../../../../../place/Place"
import { guilford } from "../Guilford"
import { GuilfordCityCode } from "../GuilfordCityCode"
import { usaCity } from "../../../UsaCity"

export let greensboro = usaCity(GuilfordCityCode.Greensboro, guilford, Place.fromDMS("36°04′48″N,79°49′10″W"))
