import { City } from "../../../../country/region/department/city/City"
import { greensboro } from "./greensboro/Greensboro"

export const guilfordCities: City[] = [
  greensboro
]
