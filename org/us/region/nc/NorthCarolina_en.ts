import { RegionMessages } from "../../../country/region/RegionMessages"
import { guilford_en } from "./guilford/Guilford_en"

export const northCarolina_en = RegionMessages.create("North Carolina", {
    guilford: guilford_en
  }
)
