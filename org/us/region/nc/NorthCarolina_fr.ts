import { RegionMessages } from "../../../country/region/RegionMessages"
import { guilford_fr } from "./guilford/Guilford_fr"

export const northCarolina_fr = RegionMessages.create("Caroline du Nord", {
    guilford: guilford_fr
  }
)
