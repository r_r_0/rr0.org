import { westmorelandCities } from "./westmoreland/WestmorelandCities"
import { City } from "../../../country/region/department/city/City"

export const pennsylvaniaCities: City[] = [
  ...westmorelandCities
]
