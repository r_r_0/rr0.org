import { Place } from "../../../../../../place/Place"
import { westmoreland } from "../Westmoreland"
import { usaCity } from "../../../UsaCity"

export let monessen = usaCity("15062", westmoreland, Place.fromDMS("40°9′15″N 79°52′58″W"))
