import { City } from "../../../../country/region/department/city/City"
import { monessen } from "./monessen/Monessen"

export const westmorelandCities: City[] = [
  monessen
]
