import { monessenMessages } from "./monessen/MonessenMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export let westmorelandMessages_en = DepartmentMessages.create(
  "Westmoreland County",
  {
    15062: monessenMessages
  }
)
