import { monessenMessages } from "./monessen/MonessenMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export let westmorelandMessages_fr = DepartmentMessages.create(
  "Comté de Westmoreland",
  {
    15062: monessenMessages
  }
)
