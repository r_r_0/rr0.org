import { RegionMessages } from "../../../country/region/RegionMessages"
import { westmorelandMessages_fr } from "./westmoreland/WestmorelandMessages_fr"

export const pennsylvania_fr = RegionMessages.create(
  "Pennsylvanie",
  {
    westmoreland: westmorelandMessages_fr
  }
)
