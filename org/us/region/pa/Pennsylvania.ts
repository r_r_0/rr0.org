import { Place } from "place/Place"
import { UsaStates } from "../UsaStates"
import { usaRegion } from "../../Usa"

export const pennsylvania = usaRegion(UsaStates.pa, Place.fromDMS("40°19′N 79°28′W"))
