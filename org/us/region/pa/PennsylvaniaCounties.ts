import { westmoreland } from "./westmoreland/Westmoreland"
import { Department } from "../../../country/region/department/Department"

export const pennsylvaniaCounties: Department[] = [
  westmoreland
]
