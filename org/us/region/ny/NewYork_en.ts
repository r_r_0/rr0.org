import { RegionMessages } from "../../../country/region/RegionMessages"
import { niagara_en } from "./niagara/Niagara_en"

export const newYork_en = RegionMessages.create("New York", {
    niagara: niagara_en
  }
)
