import { City } from "../../../country/region/department/city/City"
import { niagaraCities } from "./niagara/NiagaraCities"

export const newYorkCities: City[] = [
  ...niagaraCities
]
