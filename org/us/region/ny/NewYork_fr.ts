import { RegionMessages } from "../../../country/region/RegionMessages"
import { niagara_fr } from "./niagara/Niagara_fr"

export const newYork_fr = RegionMessages.create("New York", {
    niagara: niagara_fr
  }
)
