import { City } from "../../../../country/region/department/city/City"
import { niagaraFalls } from "./niagarafalls/NiagaraFalls"

export const niagaraCities: City[] = [
  niagaraFalls
]
