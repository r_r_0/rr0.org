import { usaRegion } from "../../Usa"
import { UsaStates } from "../UsaStates"
import { Place } from "../../../../place/Place"

export const newYork = usaRegion(UsaStates.ny, Place.fromDMS("43°0'N,75°0'W"))
