import { Department } from "../../../country/region/department/Department"
import { niagara } from "./niagara/Niagara"

export const newYorkCounties: Department[] = [
  niagara
]
