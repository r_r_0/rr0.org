import { stThomas_fr } from "./stthomas/StThomas_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const virginIslands_fr = RegionMessages.create("Îles Vierges", {
  stThomas: stThomas_fr
})
