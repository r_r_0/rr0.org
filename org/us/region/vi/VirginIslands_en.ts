import { stThomas_en } from "./stthomas/StThomas_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const virginIslands_en = RegionMessages.create("Virgin Islands", {
  stThomas: stThomas_en
})
