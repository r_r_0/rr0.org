import { Place } from "../../../../../../place/Place"
import { stThomas } from "../StThomas"
import { StThomasCityCode } from "../StThomasCityCode"
import { usaCity } from "../../../UsaCity"

export let charlotteAmalie = usaCity(StThomasCityCode.CharlotteAmalie, stThomas, Place.fromDMS("18°21′N,64°57′W"))
