import { City } from "../../../../country/region/department/city/City"
import { charlotteAmalie } from "./CharlotteAmalie/CharlotteAmalie"

export const stThomasCities: City[] = [
  charlotteAmalie
]
