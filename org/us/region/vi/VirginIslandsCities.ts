import { stThomasCities } from "./stthomas/StThomasCities"
import { City } from "../../../country/region/department/city/City"

export const virginIslandsCities: City[] = [
  ...stThomasCities
]
