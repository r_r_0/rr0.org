import { stThomas } from "./stthomas/StThomas"
import { Organization } from "../../../Organization"

export const virginIslandsCounties: Organization[] = [
  stThomas
]
