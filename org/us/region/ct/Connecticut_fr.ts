import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const connecticut_fr = RegionMessages.create("Connecticut", {
  geneva: genevaMessages_fr
})
