import { genevaCities } from "./geneva/GenevaCities"
import { City } from "../../../country/region/department/city/City"

export const connecticutCities: City[] = [
  ...genevaCities
]
