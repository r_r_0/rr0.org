import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const connecticut_en = RegionMessages.create("Connecticut", {
    geneva: genevaMessages_en
  }
)
