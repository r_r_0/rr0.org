import { geneva } from "./geneva/Geneva"
import { Organization } from "../../../Organization"

export const connecticutCounties: Organization[] = [
  geneva
]
