import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const michigan_fr = RegionMessages.create("Michigan", {
  kalamazoo: kalamazooMessages_fr
})
