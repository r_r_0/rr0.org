import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const michigan_en = new RegionMessages(["Michigan", "Mich."], {
  kalamazoo: kalamazooMessages_en
})
