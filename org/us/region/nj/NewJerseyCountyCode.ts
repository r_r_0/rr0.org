import { UsaCountyCode } from "../UsaCountyCode"

export enum NewJerseyCountyCode {
  Burlington = UsaCountyCode.burlington
}
