import { Place } from "../../../../../../place/Place"
import { burlington } from "../Burlington"
import { BurlingtonCityCode } from "../BurlingtonCityCode"
import { usaCity } from "../../../UsaCity"

export let willingboro = usaCity(BurlingtonCityCode.Willingboro, burlington, Place.fromLocation(40.02795, -74.886984))
