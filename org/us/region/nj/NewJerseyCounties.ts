import { Department } from "../../../country/region/department/Department"
import { burlington } from "./burlington/Burlington"

export const newJerseyCounties: Department[] = [
  burlington
]
