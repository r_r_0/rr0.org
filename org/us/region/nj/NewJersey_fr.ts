import { RegionMessages } from "../../../country/region/RegionMessages"
import { burlington_fr } from "./burlington/Burlington_fr"

export const newJersey_fr = RegionMessages.create("New Jersey", {
    burlington: burlington_fr
  }
)
