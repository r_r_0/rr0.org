import { RegionMessages } from "../../../country/region/RegionMessages"
import { burlington_en } from "./burlington/Burlington_en"

export const newJersey_en = RegionMessages.create("New Jersey", {
    burlington: burlington_en
  }
)
