import { City } from "../../../country/region/department/city/City"
import { burlingtonCities } from "./burlington/BurlingtonCities"

export const newJerseyCities: City[] = [
  ...burlingtonCities
]
