import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const oregon_en = RegionMessages.create("Oregon", {
  kalamazoo: kalamazooMessages_en
})
