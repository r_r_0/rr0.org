import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const oregon_fr = RegionMessages.create("Oregon", {
  kalamazoo: kalamazooMessages_fr
})
