import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const ohio_en = RegionMessages.create("Ohio", {
    geneva: genevaMessages_en
  }
)
