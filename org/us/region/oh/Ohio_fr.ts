import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const ohio_fr = RegionMessages.create("Ohio", {
  geneva: genevaMessages_fr
})
