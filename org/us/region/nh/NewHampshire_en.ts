import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const newHampshire_en = new RegionMessages(["New Hampshire"], {
  kalamazoo: kalamazooMessages_en
})
