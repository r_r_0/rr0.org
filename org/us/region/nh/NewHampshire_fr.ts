import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const newHampshire_fr = RegionMessages.create("New Hampshire", {
  kalamazoo: kalamazooMessages_fr
})
