import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const illinois_en = RegionMessages.create("Illinois", {
    geneva: genevaMessages_en
  }
)
