import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const illinois_fr = RegionMessages.create("Illinois", {
  geneva: genevaMessages_fr
})
