import { genevaMessages_en } from "./geneva/GenevaMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const colorado_en = RegionMessages.create("Colorado", {
    geneva: genevaMessages_en
  }
)
