import { genevaMessages_fr } from "./geneva/GenevaMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const colorado_fr = RegionMessages.create("Colorado", {
  geneva: genevaMessages_fr
})
