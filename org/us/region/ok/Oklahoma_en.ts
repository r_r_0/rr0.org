import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const oklahoma_en = new RegionMessages(["Oklahoma"], {
  kalamazoo: kalamazooMessages_en
})
