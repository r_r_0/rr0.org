import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const oklahoma_fr = RegionMessages.create("Oklahoma", {
  kalamazoo: kalamazooMessages_fr
})
