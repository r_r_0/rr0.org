import { kalamazoo } from "./kalamazoo/Kalamazoo"
import { Organization } from "../../../Organization"

export const oklahomaCounties: Organization[] = [
  kalamazoo
]
