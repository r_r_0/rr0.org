import { pierceMessages_fr } from "./pierce/PierceMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const washingtonState_fr = RegionMessages.create("État de Washington", {
  pierce: pierceMessages_fr
})
