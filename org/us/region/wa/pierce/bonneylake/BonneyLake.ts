import { Place } from "../../../../../../place/Place"
import { PierceCityCode } from "../PierceCityCode"
import { pierce } from "../Pierce"

import { usaCity } from "../../../UsaCity"

export let bonneyLake = usaCity(PierceCityCode.BonneyLake, pierce, Place.fromDMS("47°11′13″N 122°10′12″W"))
