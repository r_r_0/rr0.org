import { pierceCities } from "./pierce/PierceCities"
import { City } from "../../../country/region/department/city/City"

export const washingtonCities: City[] = [
  ...pierceCities
]
