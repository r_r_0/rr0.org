import { pierceMessages_en } from "./pierce/PierceMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const washingtonState_en = RegionMessages.create("Washington state", {
  pierce: pierceMessages_en
})
