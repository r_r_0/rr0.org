import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const minnesota_fr = RegionMessages.create("Minnesota", {
  kalamazoo: kalamazooMessages_fr
})
