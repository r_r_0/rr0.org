import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const minnesota_en = new RegionMessages(["Minnesota"], {
  kalamazoo: kalamazooMessages_en
})
