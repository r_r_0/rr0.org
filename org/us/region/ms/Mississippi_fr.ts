import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const mississippi_fr = RegionMessages.create("Mississippi", {
  kalamazoo: kalamazooMessages_fr
})
