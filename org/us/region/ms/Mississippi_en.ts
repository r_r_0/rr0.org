import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const mississippi_en = new RegionMessages(["Mississippi"], {
  kalamazoo: kalamazooMessages_en
})
