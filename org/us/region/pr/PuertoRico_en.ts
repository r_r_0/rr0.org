import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const puertoRico_en = new RegionMessages(["Puerto Rico"], {
  kalamazoo: kalamazooMessages_en
})
