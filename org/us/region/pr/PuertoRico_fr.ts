import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const puertoRico_fr = RegionMessages.create("Porto Rico", {
  kalamazoo: kalamazooMessages_fr
})
