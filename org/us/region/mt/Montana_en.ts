import { kalamazooMessages_en } from "./kalamazoo/KalamazooMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const montana_en = new RegionMessages(["Montana"], {
  kalamazoo: kalamazooMessages_en
})
