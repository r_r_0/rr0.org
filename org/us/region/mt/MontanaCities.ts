import { kalamazooCities } from "./kalamazoo/KalamazooCities"
import { City } from "../../../country/region/department/city/City"

export const montanaCities: City[] = [
  ...kalamazooCities
]
