import { kalamazooMessages_fr } from "./kalamazoo/KalamazooMessages_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const montana_fr = RegionMessages.create("Montana", {
  kalamazoo: kalamazooMessages_fr
})
