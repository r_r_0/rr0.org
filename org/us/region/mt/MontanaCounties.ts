import { kalamazoo } from "./kalamazoo/Kalamazoo"
import { Organization } from "../../../Organization"

export const montanaCounties: Organization[] = [
  kalamazoo
]
