import { Place } from "../../../../../../place/Place"
import { middlesex } from "../Middlesex"
import { MiddlesexCityCode } from "../MiddlesexCityCode"
import { usaCity } from "../../../UsaCity"

export let cambridge = usaCity(MiddlesexCityCode.Cambridge, middlesex, Place.fromDMS("42°22′30″N,71°06′22″W"))
