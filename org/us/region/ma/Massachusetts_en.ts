import { middlesexMessages_en } from "./middlesex/MiddlesexMessages_en"
import { RegionMessages } from "../../../country/region/RegionMessages"

export const massachusetts_en = RegionMessages.create("Massachusetts", {
    middlesex: middlesexMessages_en
  }
)
