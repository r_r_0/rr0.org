import { middlesex } from "./middlesex/Middlesex"
import { Organization } from "../../../Organization"
import { MiddlesexCityCode } from "./middlesex/MiddlesexCityCode"

export type MassachusettsCountyCode = MiddlesexCityCode
export const massachusettsCounties: Organization[] = [
  middlesex
]
