import { sanDiego } from "./sandiego/SanDiego"
import { Organization } from "../../../Organization"

export const californiaCounties: Organization[] = [
  sanDiego
]
