import { sandiegoCities } from "./sandiego/SanDiegoCities"
import { City } from "../../../country/region/department/city/City"

export const californiaCities: City[] = [
  ...sandiegoCities
]
