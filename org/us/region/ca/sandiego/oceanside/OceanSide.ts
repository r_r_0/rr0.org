import { sanDiegoCity } from "../SanDiegoCity"
import { Place } from "../../../../../../place/Place"

export let oceanSide = sanDiegoCity("92058", Place.fromDMS("33° 12′ 42″ N, 117° 19′ 33″ O"))
