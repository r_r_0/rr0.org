import { oceanSide } from "./oceanside/OceanSide"
import { campPendleton } from "./camppendleton/CampPendleton"
import { City } from "../../../../country/region/department/city/City"

export const sandiegoCities: City[] = [
  oceanSide,
  campPendleton
]
