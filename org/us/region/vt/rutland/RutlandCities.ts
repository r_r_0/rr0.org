import { City } from "../../../../country/region/department/city/City"
import { rutland } from "./Rutland/Rutland"

export const rutlandCities: City[] = [
  rutland
]
