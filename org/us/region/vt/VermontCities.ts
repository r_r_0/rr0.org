import { rutlandCities } from "./rutland/RutlandCities"
import { City } from "../../../country/region/department/city/City"

export const vermontCities: City[] = [
  ...rutlandCities
]
