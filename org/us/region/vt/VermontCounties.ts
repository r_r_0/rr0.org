import { rutlandCounty } from "./rutland/Rutland"
import { Organization } from "../../../Organization"

export const vermontCounties: Organization[] = [
  rutlandCounty
]
