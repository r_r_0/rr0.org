import { rutlandCounty_fr } from "./rutland/Rutland_fr"
import { RegionMessages } from "../../../country/region/RegionMessages"
import { UsaCountyCode } from "../UsaCountyCode"

export const vermont_fr = RegionMessages.create("Vermont", {
  [UsaCountyCode.rutland]: rutlandCounty_fr
})
