import { Place } from "../../../../../../../place/Place"
import { sucha } from "../Sucha"
import { spainCity } from "../../../../../es/region/SpainCity"
import { SuchaCityCode } from "../SuchaCityCode"

export const jordanow = spainCity(SuchaCityCode.Jordanow, sucha, Place.fromDMS("49°40′N,19°50′E"))
