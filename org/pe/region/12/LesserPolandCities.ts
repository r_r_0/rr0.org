import { City } from "../../../../country/region/department/city/City"
import { suchaCities } from "./sucha/SuchaCities"

export const lesserPolandCities: City[] = [
  ...suchaCities
]
