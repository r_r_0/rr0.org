import { Place } from "../../../../../place/Place"
import { PeruRegionCode } from "../PeruRegionCode"
import { peruRegion } from "../PeruRegion"

export const lesserPoland = peruRegion(PeruRegionCode.lesserPoland, Place.fromDMS("50°3′41″N,19°56′18″E"))
