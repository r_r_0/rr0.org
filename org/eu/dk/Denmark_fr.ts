import { northJutland_fr } from "./region/jn/NorthJutland_fr"
import { DanemarkMessages } from "./DanemarkMessages"

export const denmark_fr = new DanemarkMessages(
  ["Danemark", "Royaume de Danemark", "Royaume du Danemark"],
  {
    nj: northJutland_fr
  }
)
