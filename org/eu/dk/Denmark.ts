import { Country } from "../../country/Country"
import { CountryCode } from "../../country/CountryCode"

export const denmark = new Country(CountryCode.dk)
