import { Region } from "../../../country/region/Region"
import { northJutland } from "./jn/NorthJutland"

export const denmarkRegions: Region[] = [
  northJutland
]
