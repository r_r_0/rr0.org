import { NorthJutlandCityCode } from "./NorthJutlandCityCode"
import { CityMessages } from "../../../../country/region/department/city/CityMessages"

export type NorthJutlandMessages = { [key in NorthJutlandCityCode]: CityMessages }
