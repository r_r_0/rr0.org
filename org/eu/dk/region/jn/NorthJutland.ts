import { denmarkRegion } from "../DenmarkRegion"
import { DenmarkRegionCode } from "../DenmarkRegionCode"
import { Place } from "../../../../../place/Place"

export const northJutland = denmarkRegion(DenmarkRegionCode.nj, Place.fromDMS("57°0'N,10°0'E"))
