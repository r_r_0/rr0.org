import { City } from "../../../../country/region/department/city/City"
import { aalborg } from "./aalborg/Aalborg"

export const northJutlandCities: City[] = [
  aalborg
]
