import { northJutlandCities } from "./region/jn/NorthJutlandCities"
import { City } from "../../country/region/department/city/City"

export const denmarkCities: City[] = [
  ...northJutlandCities
]
