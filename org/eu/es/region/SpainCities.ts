import { City } from "../../../country/region/department/city/City"
import { cataloniaCities } from "./cat/CataloniaCities"

export const spainCities: City[] = [
  ...cataloniaCities
]
