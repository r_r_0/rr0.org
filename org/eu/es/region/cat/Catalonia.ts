import { Place } from "../../../../../place/Place"
import { SpainRegionCode } from "../SpainRegionCode"
import { spainRegion } from "../SpainRegion"

export const catalonia = spainRegion(SpainRegionCode.ct, Place.fromDMS("42°00′00″N,1°10′00″E"))
