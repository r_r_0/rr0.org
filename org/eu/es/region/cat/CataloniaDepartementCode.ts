import { SpainDepartementCode } from "../SpainDepartementCode"

export enum CataloniaDepartementCode {
  Lleida = SpainDepartementCode.Lerida,
}
