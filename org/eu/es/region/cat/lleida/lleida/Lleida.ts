import { Place } from "../../../../../../../place/Place"
import { spainCity } from "../../../SpainCity"
import { LleidaCityCode } from "../LleidaCityCode"
import { lleida } from "../Lleida"

export const lleidaCity = spainCity(LleidaCityCode.Lleida, lleida, Place.fromDMS("41°37′00″N,00°38′00″E"))
