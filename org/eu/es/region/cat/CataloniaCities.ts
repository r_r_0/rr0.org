import { lleidaCities } from "./lleida/LleidaCities"
import { City } from "../../../../country/region/department/city/City"

export const cataloniaCities: City[] = [
  ...lleidaCities
]
