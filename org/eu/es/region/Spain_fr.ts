import { CountryMessages } from "../../../country/CountryMessages"
import { cataloniaMessages_fr } from "./cat/CataloniaMessages_fr"

export const spain_fr = CountryMessages.create("Espagne", {
  ct: cataloniaMessages_fr
})
