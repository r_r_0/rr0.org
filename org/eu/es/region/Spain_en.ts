import { CountryMessages } from "../../../country/CountryMessages"
import { cataloniaMessages_en } from "./cat/CataloniaMessages_en"

export const spain_en = CountryMessages.create("Spain", {
  ct: cataloniaMessages_en
})
