import { RegionMessages } from "../../../../country/region/RegionMessages"
import { pkMessages_en } from "./pk/PkMessages_en"

export const northKareliaMessages_en = RegionMessages.create("North Karelia", {
  pk: pkMessages_en
})
