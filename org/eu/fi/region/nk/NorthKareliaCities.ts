import { pkCities } from "./pk/PkCities"
import { City } from "../../../../country/region/department/city/City"

export const northKareliaCities: City[] = [
  ...pkCities
]
