import { finlandRegion } from "../FinlandRegion"
import { FinlandRegionCode } from "../FinlandRegionCode"
import { Place } from "../../../../../place/Place"

export const northKarelia = finlandRegion(FinlandRegionCode.nk, Place.fromDMS("63°00′N 30°00′E"))
