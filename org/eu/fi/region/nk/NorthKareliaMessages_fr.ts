import { RegionMessages } from "../../../../country/region/RegionMessages"
import { pkMessages_fr } from "./pk/PkMessages_fr"

export const northKareliaMessages_fr = RegionMessages.create("Carélie du Nord", {
  pk: pkMessages_fr
})
