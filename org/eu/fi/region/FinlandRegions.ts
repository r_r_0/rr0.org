import { Region } from "../../../country/region/Region"
import { northKarelia } from "./nk/NorthKarelia"
import { pirkanmaa } from "./p/Pirkanmaa"
import { southSavo } from "./ss/SouthSavo"

export const finlandRegions: Region[] = [
  northKarelia,
  pirkanmaa,
  southSavo
]
