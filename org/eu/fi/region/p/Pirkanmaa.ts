import { finlandRegion } from "../FinlandRegion"
import { FinlandRegionCode } from "../FinlandRegionCode"
import { Place } from "../../../../../place/Place"

export const pirkanmaa = finlandRegion(FinlandRegionCode.p, Place.fromDMS("63°00′N 30°00′E"))
