import { DepartmentMessages } from "../../../../../country/region/department/DepartmentMessages"
import { nwpCityMessages } from "./NwpCityMessages"

export const nwpMessages_en = DepartmentMessages.create("North Western Pirkanmaa", nwpCityMessages)
