import { DepartmentMessages } from "../../../../../country/region/department/DepartmentMessages"
import { nwpCityMessages } from "./NwpCityMessages"

export const nwpMessages_fr = DepartmentMessages.create("Pirkanmaa du Nord-Ouest", nwpCityMessages)
