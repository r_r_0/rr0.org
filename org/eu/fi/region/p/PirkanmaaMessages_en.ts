import { RegionMessages } from "../../../../country/region/RegionMessages"
import { nwpMessages_en } from "./nwp/NwpMessages_en"

export const pirkanmaaMessages_en = RegionMessages.create("North Karelia", {
  nwp: nwpMessages_en
})
