import { Department } from "../../../../country/region/department/Department"
import { nwp } from "./nwp/Nwp"

export const pirkanmaaDepartments: Department[] = [
  nwp
]
