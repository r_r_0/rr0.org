import { nwpCities } from "./nwp/NwpCities"
import { City } from "../../../../country/region/department/city/City"

export const pirkanmaaCities: City[] = [
  ...nwpCities
]
