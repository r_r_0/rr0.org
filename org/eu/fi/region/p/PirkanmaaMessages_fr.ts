import { RegionMessages } from "../../../../country/region/RegionMessages"
import { nwpMessages_fr } from "./nwp/NwpMessages_fr"

export const pirkanmaaMessages_fr = RegionMessages.create("Carélie du Nord", {
  nwp: nwpMessages_fr
})
