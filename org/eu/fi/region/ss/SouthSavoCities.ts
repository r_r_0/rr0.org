import { pieksamakiCities } from "./Pieksamaki/PieksamakiCities"
import { City } from "../../../../country/region/department/city/City"

export const southSavoCities: City[] = [
  ...pieksamakiCities
]
