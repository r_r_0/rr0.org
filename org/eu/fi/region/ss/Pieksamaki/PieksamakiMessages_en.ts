import { DepartmentMessages } from "../../../../../country/region/department/DepartmentMessages"
import { pieksamakiCityMessages } from "./PieksamakiCityMessages"

export const pieksamakiMessages_en = DepartmentMessages.create("Sub-region of Pieksämäki", pieksamakiCityMessages)
