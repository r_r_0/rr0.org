import { DepartmentMessages } from "../../../../../country/region/department/DepartmentMessages"
import { pieksamakiCityMessages } from "./PieksamakiCityMessages"

export const pieksamakiMessages_fr = DepartmentMessages.create("Sous-région de Pieksämäki", pieksamakiCityMessages)
