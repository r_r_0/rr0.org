import { finlandRegion } from "../FinlandRegion"
import { FinlandRegionCode } from "../FinlandRegionCode"
import { Place } from "../../../../../place/Place"

export const southSavo = finlandRegion(FinlandRegionCode.ss, Place.fromDMS("62°0′N 27°30′E"))
