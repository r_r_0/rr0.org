import { Department } from "../../../../country/region/department/Department"
import { pieksamakiDep } from "./Pieksamaki/PieksamakiDep"

export const southSavoDepartments: Department[] = [
  pieksamakiDep
]
