import { SouthSavoDepartmentCode } from "./ss/SouthSavoDepartmentCode"

export enum FinlandDepartementCode {
  /**
   * Pielinen Karelia
   */
  pk = "pk",

  /**
   *
   */
  nwp = "nwp",

  /**
   * pieksamaki sub-region
   */
  Pieksamaki = SouthSavoDepartmentCode.Pieksamaki
}
