import { Country } from "../../country/Country"
import { CountryCode } from "../../country/CountryCode"

export const finland = new Country(CountryCode.fi)
