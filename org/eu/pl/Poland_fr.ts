import { CountryMessages } from "../../country/CountryMessages"
import { lesserPolandMessages_fr } from "./region/12/LesserPollandMessages_fr"

export const poland_fr = new CountryMessages(["Pologne"], {
  12: lesserPolandMessages_fr
})
