import { lesserPolandCities } from "./12/LesserPolandCities"
import { City } from "../../../country/region/department/city/City"

export const polandCities: City[] = [
  ...lesserPolandCities
]
