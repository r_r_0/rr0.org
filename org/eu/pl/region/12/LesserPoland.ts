import { Place } from "../../../../../place/Place"
import { PolandRegionCode } from "../PolandRegionCode"
import { polandRegion } from "../PolandRegion"

export const lesserPoland = polandRegion(PolandRegionCode.lesserPoland, Place.fromDMS("50°3′41″N,19°56′18″E"))
