import { CountryMessages } from "../../country/CountryMessages"
import { lesserPolandMessages_en } from "./region/12/LesserPollandMessages_en"

export const poland_en = new CountryMessages(["Poland"], {
  12: lesserPolandMessages_en
})
