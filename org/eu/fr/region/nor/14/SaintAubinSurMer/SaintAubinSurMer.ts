import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CalvadosCityCode } from "../CalvadosCityCode"

export const saintAubinSurMer14 = franceCity(CalvadosCityCode.SaintAubinSurMer, Place.fromDMS("49° 19′ 45″N,0°23′19″W"))
