import { City } from "../../../../../country/region/department/city/City"
import { pontLEveque14 } from "./PontLEveque/PontLEveque"
import { saintAubinSurMer14 } from "./SaintAubinSurMer/SaintAubinSurMer"

export const calvadosCities: City[] = [
  pontLEveque14,
  saintAubinSurMer14
]
