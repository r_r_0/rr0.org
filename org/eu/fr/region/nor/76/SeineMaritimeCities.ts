import { City } from "../../../../../country/region/department/city/City"
import { londe76 } from "./Londe/Londe"
import { crielSurMer } from "./CrielSurMer/CrielSurMer"
import { dieppe } from "./Dieppe/Dieppe"
import { saintAubinSurMer76 } from "./SaintAubinSurMer/SaintAubinSurMer"

export const seineMaritimeCities: City[] = [
  crielSurMer,
  dieppe,
  londe76,
  saintAubinSurMer76
]
