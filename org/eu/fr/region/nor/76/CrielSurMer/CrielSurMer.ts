import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SeineMaritimeCityCode } from "../SeineMaritimeCityCode"

export const crielSurMer = franceCity(SeineMaritimeCityCode.CrielSurMer, Place.fromDMS("50° 01′ 00″ N, 1° 19′ 06″ E"))
