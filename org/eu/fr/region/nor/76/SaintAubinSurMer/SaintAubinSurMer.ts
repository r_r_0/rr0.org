import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SeineMaritimeCityCode } from "../SeineMaritimeCityCode"

export const saintAubinSurMer76 = franceCity(SeineMaritimeCityCode.SaintAubinSurMer,
  Place.fromDMS("49°19′45″N,0°23′19″W"))
