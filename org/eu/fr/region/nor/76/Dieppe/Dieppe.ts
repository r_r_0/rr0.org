import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SeineMaritimeCityCode } from "../SeineMaritimeCityCode"

export const dieppe = franceCity(SeineMaritimeCityCode.Dieppe, Place.fromDMS("49° 55′ 20″N, 1° 04′ 43″E"))
