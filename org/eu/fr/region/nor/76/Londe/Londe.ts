import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SeineMaritimeCityCode } from "../SeineMaritimeCityCode"

export const londe76 = franceCity(SeineMaritimeCityCode.Londe, Place.fromDMS("49°18′24″N,0°57′14″E"))
