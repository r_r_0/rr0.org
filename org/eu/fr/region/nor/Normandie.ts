import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const normandie = franceRegion(FranceRegionCode.nor, Place.fromLocation(49.186389, 0.352778))
