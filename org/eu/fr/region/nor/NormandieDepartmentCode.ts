import { FranceDepartementCode } from "../FranceDepartementCode"

export enum NormandieDepartmentCode {
  Calvados = FranceDepartementCode.Calvados,
  SeineMaritime = FranceDepartementCode.SeineMaritime,
}
