import { calvados } from "./14/Calvados"
import { Department } from "../../../../country/region/department/Department"
import { seineMaritime } from "./76/SeineMaritime"

export const normandieDepartments: Department[] = [
  calvados,
  seineMaritime
]
