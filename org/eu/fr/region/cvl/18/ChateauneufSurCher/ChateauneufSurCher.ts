import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CherCityCode } from "../CherCityCode"

export const chateauneufSurCher = franceCity(CherCityCode.ChateauneufSurCher, Place.fromDMS("46°51′30″N,2°19′02″E"))
