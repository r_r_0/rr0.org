import { City } from "../../../../../country/region/department/city/City"
import { chateauneufSurCher } from "./ChateauneufSurCher/ChateauneufSurCher"

export const cherCities: City[] = [
  chateauneufSurCher
]
