import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoiretCityCode } from "../LoiretCityCode"

export const chateauRenard = franceCity(LoiretCityCode.ChateauRenard, Place.fromDMS("47°55′53″N,2°55′41″E"))
