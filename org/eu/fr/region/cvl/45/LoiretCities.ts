import { City } from "../../../../../country/region/department/city/City"
import { chateauRenard } from "./chateaurenard/ChateauRenard"
import { bou45 } from "./bou/Bou"

export const loiretCities: City[] = [
  bou45,
  chateauRenard
]
