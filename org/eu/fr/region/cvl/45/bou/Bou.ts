import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoiretCityCode } from "../LoiretCityCode"

export const bou45 = franceCity(LoiretCityCode.Bou, Place.fromDMS("47°52′27″N,2°02′54″E"))
