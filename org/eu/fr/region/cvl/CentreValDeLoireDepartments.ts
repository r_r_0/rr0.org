import { indre } from "./36/Indre"
import { loiret } from "./45/Loiret"
import { Department } from "../../../../country/region/department/Department"
import { indreEtLoire } from "./37/IndreEtLoire"
import { cher } from "./18/Cher"

export const centreValDeLoireDepartments: Department[] = [
  cher,
  indre,
  indreEtLoire,
  loiret
]
