import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IndreEtLoireCityCode } from "../IndreEtLoireCityCode"

export const chambourgSurIndre = franceCity(IndreEtLoireCityCode.ChambourgSurIndre,
  Place.fromDMS("47°10′56″N,0°58′05″E"))
