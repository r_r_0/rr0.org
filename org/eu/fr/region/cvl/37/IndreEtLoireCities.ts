import { City } from "../../../../../country/region/department/city/City"
import { blere } from "./Blere/Blere"
import { monts37 } from "./Monts/Monts"
import { chambourgSurIndre } from "./ChambourgSurIndre/ChambourgSurIndre"

export const indreEtLoireCities: City[] = [
  blere,
  chambourgSurIndre,
  monts37
]
