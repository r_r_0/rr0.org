import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IndreEtLoireCityCode } from "../IndreEtLoireCityCode"

export const monts37 = franceCity(IndreEtLoireCityCode.Monts, Place.fromDMS("47°16′41″N,0°37′31″E"))
