import { FranceDepartementCode } from "../FranceDepartementCode"

export enum CentreValDeLoireDepartementCode {
  Cher = FranceDepartementCode.Cher,
  Indre = FranceDepartementCode.Indre,
  IndreEtLoire = FranceDepartementCode.IndreEtLoire,
  Loiret = FranceDepartementCode.Loiret
}
