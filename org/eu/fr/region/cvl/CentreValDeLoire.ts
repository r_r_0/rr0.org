import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const centreValDeLoire = franceRegion(FranceRegionCode.cvl, Place.fromDMS("47°30′N,1°45′E"))
