import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IndreCityCode } from "../IndreCityCode"

export const issoudun = franceCity(IndreCityCode.Issoudun, Place.fromDMS("46°57′39″N,1°59′40″E"))
