import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HauteVienneCityCode } from "../HauteVienneCityCode"

export const jabreilles = franceCity(HauteVienneCityCode.Jabreilles, Place.fromDMS("46°01′05″N,1°30′58″E"))
