import { jabreilles } from "./jabreilles/Jabreilles"
import { City } from "../../../../../country/region/department/city/City"

export const hauteVienneCities: City[] = [
  jabreilles
]
