import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PyreneesOrientalesCityCode } from "../PyreneesOrientalesCityCode"

export const planes = franceCity(PyreneesOrientalesCityCode.Planes, Place.fromDMS("42° 29′ 35″ N, 2° 08′ 25″E"))
