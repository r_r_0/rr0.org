import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PyreneesOrientalesCityCode } from "../PyreneesOrientalesCityCode"

export const portePuymorens = franceCity(PyreneesOrientalesCityCode.PortePuymorens,
  Place.fromDMS("42°32′57″N,1°49′57″E"))
