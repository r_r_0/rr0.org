import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PyreneesOrientalesCityCode } from "../PyreneesOrientalesCityCode"

export const thuir = franceCity(PyreneesOrientalesCityCode.Thuir, Place.fromDMS("42°37′59″N,2°45′26″E"))
