import { portePuymorens } from "./PortePuymorens/PortePuymorens"
import { thuir } from "./Thuir/Thuir"
import { planes } from "./Planes/Planes"
import { City } from "../../../../../country/region/department/city/City"

export const pyreneesOrientalesCities: City[] = [
  planes,
  portePuymorens,
  thuir
]
