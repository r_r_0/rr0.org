import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AriegeCityCode } from "../AriegeCityCode"

export const cos = franceCity(AriegeCityCode.Cos, Place.fromDMS("42°51′07″N,2°36′11″E"))
