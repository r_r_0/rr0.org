import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { GardCityCode } from "../GardCityCode"

export const stGilles = franceCity(GardCityCode.SaintGilles, Place.fromDMS("43°40′43″N,4°25′54″E"))
