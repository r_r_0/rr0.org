import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { GardCityCode } from "../GardCityCode"

export const ledignan = franceCity(GardCityCode.Ledignan, Place.fromDMS("43°59′22″N,4°06′26″E"))
