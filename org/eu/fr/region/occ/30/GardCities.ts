import { ledignan } from "./ledignan/Ledignan"
import { Organization } from "../../../../../Organization"
import { stGilles } from "./SaintGilles/StGilles"

export const gardCities: Organization[] = [
  ledignan,
  stGilles
]
