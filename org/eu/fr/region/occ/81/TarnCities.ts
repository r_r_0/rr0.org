import { albi } from "./Albi/Albi"
import { Organization } from "../../../../../Organization"
import { castelnauDeMontmiral } from "./CastelnauDeMontmiral/CastelnauDeMontmiral"

export const tarnCities: Organization[] = [
  albi,
  castelnauDeMontmiral
]
