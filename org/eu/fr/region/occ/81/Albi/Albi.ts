import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { TarnCityCode } from "../TarnCityCode"

export const albi = franceCity(TarnCityCode.Albi, Place.fromDMS("43°55′44″N,2°08′47″E"))
