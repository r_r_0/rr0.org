import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { TarnCityCode } from "../TarnCityCode"

export const castelnauDeMontmiral = franceCity(TarnCityCode.CastelnauDeMontmiral, Place.fromDMS("43°57′59″N,1°49′18″E"))
