import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AudeCityCode } from "../AudeCityCode"

export const cucugnan = franceCity(AudeCityCode.Cucugnan, Place.fromDMS("42°51′07″N,2°36′11″E"))
