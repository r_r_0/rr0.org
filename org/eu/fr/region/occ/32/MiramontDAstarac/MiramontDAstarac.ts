import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { GersCityCode } from "../GersCityCode"

export const miramontDAstarac = franceCity(GersCityCode.MiramontDAstarac, Place.fromDMS("43° 32′ 51″N, 0° 28′12″E"))
