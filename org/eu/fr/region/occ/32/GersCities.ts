import { estang } from "./Estang/Estang"
import { City } from "../../../../../country/region/department/city/City"
import { miramontDAstarac } from "./MiramontDAstarac/MiramontDAstarac"
import { condom } from "./Condom/Condom"

export const gersCities: City[] = [
  condom,
  estang,
  miramontDAstarac
]
