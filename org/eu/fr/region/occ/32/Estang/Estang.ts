import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { GersCityCode } from "../GersCityCode"

export const estang = franceCity(GersCityCode.Estang, Place.fromDMS("43° 52′ 03″ N, 0° 06′ 27″ O"))
