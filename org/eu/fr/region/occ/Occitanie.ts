import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const occitanie = franceRegion(FranceRegionCode.occ, Place.fromDMS("43°38′56″N,2°20′37″E"))
