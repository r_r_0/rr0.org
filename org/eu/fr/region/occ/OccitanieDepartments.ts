import { gers } from "./32/Gers"
import { herault } from "./34/Herault"
import { tarnEtGaronne } from "./82/TarnEtGaronne"
import { hauteGaronne } from "./31/HauteGaronne"
import { aude } from "./11/Aude"
import { tarn } from "./81/Tarn"
import { Organization } from "../../../../Organization"
import { gard } from "./30/Gard"
import { pyreneesOrientales } from "./66/PyreneesOrientales"
import { ariege } from "./09/Ariege"
import { hauteVienne } from "./87/HauteVienne"
import { aveyron } from "./12/Aveyron"
import { hautesPyrenees } from "./65/HautesPyrenees"

export const occitanieDepartments: Organization[] = [
  ariege,
  aude,
  aveyron,
  gers,
  gard,
  hauteGaronne,
  hauteVienne,
  hautesPyrenees,
  herault,
  tarn,
  tarnEtGaronne,
  pyreneesOrientales
]
