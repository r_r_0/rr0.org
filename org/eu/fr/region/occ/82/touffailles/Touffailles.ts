import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { TarnEtGaronneCityCode } from "../TarnEtGaronneCityCode"

export const touffailles = franceCity(TarnEtGaronneCityCode.Touffailles, Place.fromDMS("44°16′26″N,1°03′05″E"))
