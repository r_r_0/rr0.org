import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HeraultCityCode } from "../HeraultCityCode"

export const montpellier = franceCity(HeraultCityCode.Montpellier, Place.fromDMS("43°52′03″N,0°06′27″W"))
