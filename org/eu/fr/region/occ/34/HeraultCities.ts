import { montpellier } from "./Montpellier/Montpellier"
import { Organization } from "../../../../../Organization"
import { saintGeniesDesMourgues } from "./SaintGeniesDesMourgues/SaintGeniesDesMourgues"

export const heraultCities: Organization[] = [
  saintGeniesDesMourgues,
  montpellier
]
