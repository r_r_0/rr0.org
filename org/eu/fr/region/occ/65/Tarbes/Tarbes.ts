import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HautesPyreneesCityCode } from "../HautesPyreneesCityCode"

export const tarbes = franceCity(HautesPyreneesCityCode.Tarbes, Place.fromDMS("43°13′51″N,0°04′21″E"))
