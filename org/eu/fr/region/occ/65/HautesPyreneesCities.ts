import { tarbes } from "./Tarbes/Tarbes"
import { City } from "../../../../../country/region/department/city/City"

export const hautesPyreneesCities: City[] = [
  tarbes
]
