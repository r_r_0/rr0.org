import { OccitanieDepartmentsMessages, occitanieDepartmentsMessages } from "./OccitanieDepartmentsMessages"
import { RegionMessages } from "../../../../country/region/RegionMessages"

export const occitanieMessages_en = RegionMessages.create<OccitanieDepartmentsMessages>(
  "Occitania", occitanieDepartmentsMessages
)
