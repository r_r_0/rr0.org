import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HauteGaronneCityCode } from "../HauteGaronneCityCode"

export const stPlancard = franceCity(HauteGaronneCityCode.StPlancard, Place.fromDMS("43°10′16″N,0°34′30″E"))
