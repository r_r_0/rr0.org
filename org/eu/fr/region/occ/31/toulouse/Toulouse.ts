import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HauteGaronneCityCode } from "../HauteGaronneCityCode"

export const toulouse = franceCity(HauteGaronneCityCode.Toulouse, Place.fromDMS("43°36′16″N,1°26′38″E"))
