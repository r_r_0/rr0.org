import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HauteGaronneCityCode } from "../HauteGaronneCityCode"

export const loudet = franceCity(HauteGaronneCityCode.Loudet, Place.fromDMS("43°08′57″N,0°34′24″E"))
