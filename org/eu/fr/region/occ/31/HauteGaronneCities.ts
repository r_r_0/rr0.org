import { loudet } from "./loudet/Loudet"
import { stPlancard } from "./stplancard/StPlancard"
import { Organization } from "../../../../../Organization"
import { toulouse } from "./toulouse/Toulouse"

export const hauteGaronneCities: Organization[] = [
  loudet,
  stPlancard,
  toulouse
]
