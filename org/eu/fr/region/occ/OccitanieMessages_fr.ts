import { OccitanieDepartmentsMessages, occitanieDepartmentsMessages } from "./OccitanieDepartmentsMessages"
import { RegionMessages } from "../../../../country/region/RegionMessages"

export const occitanieMessages_fr = RegionMessages.create<OccitanieDepartmentsMessages>(
  "Occitanie", occitanieDepartmentsMessages
)
