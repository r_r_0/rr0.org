import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AveyronCityCode } from "../AveyronCityCode"

export const espalion = franceCity(AveyronCityCode.Espalion, Place.fromDMS("42°51′07″N,2°36′11″E"))
