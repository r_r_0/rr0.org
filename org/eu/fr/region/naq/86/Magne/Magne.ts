import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VienneCityCode } from "../VienneCityCode"

export const magne = franceCity(VienneCityCode.Magne, Place.fromDMS("46° 21′ 28″N, 0° 23′ 37″E"))
