import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { DordogneCityCode } from "../DordogneCityCode"

export const eymet = franceCity(DordogneCityCode.Eymet, Place.fromDMS("44° 40′ 07″ N, 0° 23′ 56″ E"))
