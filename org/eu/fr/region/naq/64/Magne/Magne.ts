import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PyreneesAtlantiquesCityCode } from "../PyreneesAtlantiquesCityCode"

export const magne = franceCity(PyreneesAtlantiquesCityCode.Magne, Place.fromDMS("46° 21′ 28″N, 0° 23′ 37″E"))
