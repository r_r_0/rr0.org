import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CorrezeCityCode } from "../CorrezeCityCode"

export const larochePresFeyt = franceCity(CorrezeCityCode.LarochePresFeyt, Place.fromDMS(`45°42′21″N,2°30′27″E`))
