import { larochePresFeyt } from "./LarochePresFeyt/LarochePresFeyt"
import { City } from "../../../../../country/region/department/city/City"
import { briveLaGaillarde } from "./BriveLaGaillarde/BriveLaGaillarde"

export const correzeCities: City[] = [
  briveLaGaillarde,
  larochePresFeyt
]
