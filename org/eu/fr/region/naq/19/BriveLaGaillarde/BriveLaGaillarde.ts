import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CorrezeCityCode } from "../CorrezeCityCode"

export const briveLaGaillarde = franceCity(CorrezeCityCode.BriveLaGaillarde, Place.fromDMS(`45°09′30″N,1°31′55″E`))
