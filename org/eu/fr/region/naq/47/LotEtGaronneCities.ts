import { City } from "../../../../../country/region/department/city/City"
import { laroqueTimbaut } from "./LaroqueTimbaut/LaroqueTimbaut"
import { pontDuCasse } from "./PontDuCasse/PontDuCasse"

export const lotEtGaronneCities: City[] = [
  laroqueTimbaut,
  pontDuCasse
]
