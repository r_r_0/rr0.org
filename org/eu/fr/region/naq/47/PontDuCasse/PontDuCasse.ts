import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LotEtGaronneCityCode } from "../LotEtGaronneCityCode"

export const pontDuCasse = franceCity(LotEtGaronneCityCode.PontDuCasse, Place.fromDMS("44° 13′ 57″ N, 0° 40′ 55″E"))
