import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LotEtGaronneCityCode } from "../LotEtGaronneCityCode"

export const laroqueTimbaut = franceCity(LotEtGaronneCityCode.LaroqueTimbaut, Place.fromDMS("43°53′29″N,0°29′58″O"))
