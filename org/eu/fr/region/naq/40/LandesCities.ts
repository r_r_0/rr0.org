import { montDeMarsan } from "./MontDeMarsan/MontDeMarsan"
import { City } from "../../../../../country/region/department/city/City"

export const landesCities: City[] = [
  montDeMarsan
]
