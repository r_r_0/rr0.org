import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LandesCityCode } from "../LandesCityCode"

export const montDeMarsan = franceCity(LandesCityCode.MontDeMarsan, Place.fromDMS("43°53′29″N,0°29′58″O"))
