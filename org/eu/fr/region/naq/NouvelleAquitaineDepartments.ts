import { creuse } from "./23/Creuse"
import { charente } from "./16/Charente"
import { landes } from "./40/Landes"
import { charenteMaritime } from "./17/CharenteMaritime"
import { vienne } from "./86/Vienne"
import { Organization } from "../../../../Organization"
import { correze } from "./19/Correze"
import { gironde } from "./33/Gironde"
import { lotEtGaronne } from "./47/LotEtGaronne"
import { dordogne } from "./24/Dordogne"
import { pyreneesAtlantiques } from "./64/PyreneesAtlantiques"

export const nouvelleAquitaineDepartments: Organization[] = [
  charente,
  charenteMaritime,
  correze,
  creuse,
  dordogne,
  gironde,
  landes,
  lotEtGaronne,
  pyreneesAtlantiques,
  vienne
]
