import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { GirondeCityCode } from "../GirondeCityCode"

export const etauliers = franceCity(GirondeCityCode.Etauliers, Place.fromDMS("45°13′29″N,0°34′21″O"))
