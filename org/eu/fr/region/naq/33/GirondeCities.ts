import { saintAndreDeCubzac } from "./saintandredecubzac/SaintAndreDeCubzac"
import { City } from "../../../../../country/region/department/city/City"
import { etauliers } from "./etauliers/Etauliers"

export const girondeCities: City[] = [
  etauliers,
  saintAndreDeCubzac
]
