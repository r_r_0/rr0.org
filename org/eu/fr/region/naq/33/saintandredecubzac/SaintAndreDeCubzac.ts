import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { GirondeCityCode } from "../GirondeCityCode"

export const saintAndreDeCubzac = franceCity(GirondeCityCode.SaintAndreDeCubzac, Place.fromDMS("44°59′44″N,0°26′41″O"))
