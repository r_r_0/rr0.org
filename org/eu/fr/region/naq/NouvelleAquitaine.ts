import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const nouvelleAquitaine = franceRegion(FranceRegionCode.naq, Place.fromLocation(44.836667, 0.588889))
