import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CreuseCityCode } from "../CreuseCityCode"

export const chambonSurVoueize23 = franceCity(CreuseCityCode.ChambonSurVoueize, Place.fromLocation(46.19, 2.425833))
