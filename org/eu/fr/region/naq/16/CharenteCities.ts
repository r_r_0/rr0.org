import { montigne16 } from "./Montigne/Montigne"
import { Organization } from "../../../../../Organization"
import { villeboisLavalette16 } from "./Villeboislavalette/VilleboisLavalette"
import { rouillac16 } from "./Rouillac/Rouillac"
import { angouleme16 } from "./Angouleme/Angouleme"

export const charenteCities: Organization[] = [
  angouleme16,
  montigne16,
  rouillac16,
  villeboisLavalette16
]
