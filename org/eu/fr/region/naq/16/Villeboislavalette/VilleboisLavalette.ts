import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CharenteCityCode } from "../CharenteCityCode"

export const villeboisLavalette16 = franceCity(CharenteCityCode.VilleboisLavalette,
  Place.fromDMS("45°29′01″N,0°16′50″E"))
