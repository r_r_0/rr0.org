import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CharenteCityCode } from "../CharenteCityCode"

export const rouillac16 = franceCity(CharenteCityCode.Rouillac, Place.fromDMS("45° 46′ 36″N, 0° 03′ 43″W"))
