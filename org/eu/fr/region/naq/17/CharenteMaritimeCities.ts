import { stMartinDeRe17 } from "./StMartinDeRe/StMartinDeRe"
import { City } from "../../../../../country/region/department/city/City"
import { saintJeanAngely } from "./SaintJeanAngely/SaintJeanAngely"
import { matha } from "./Matha/Matha"
import { saujon } from "./Saujon/Saujon"

export const charenteMaritimeCities: City[] = [
  matha,
  saintJeanAngely,
  saujon,
  stMartinDeRe17
]
