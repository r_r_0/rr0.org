import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CharenteMaritimeCityCode } from "../CharenteMaritimeCityCode"

export const matha = franceCity(CharenteMaritimeCityCode.Matha,
  Place.fromDMS(`45° 52′ 06″ N, 0° 19′ 08″W`))
