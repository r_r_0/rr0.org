import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CharenteMaritimeCityCode } from "../CharenteMaritimeCityCode"

export const saintJeanAngely = franceCity(CharenteMaritimeCityCode.SaintJeanAngely,
  Place.fromDMS(`45°56′48″N,0°31′46″W`))
