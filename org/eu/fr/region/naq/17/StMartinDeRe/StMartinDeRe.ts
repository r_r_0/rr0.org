import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CharenteMaritimeCityCode } from "../CharenteMaritimeCityCode"

export const stMartinDeRe17 = franceCity(CharenteMaritimeCityCode.StMartinDeRe, Place.fromDMS(`46°12′11″N,1°22′02″O`))
