import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CharenteMaritimeCityCode } from "../CharenteMaritimeCityCode"

export const saujon = franceCity(CharenteMaritimeCityCode.Saujon,
  Place.fromDMS(`45° 40′ 17″N,0°55′40″W`))
