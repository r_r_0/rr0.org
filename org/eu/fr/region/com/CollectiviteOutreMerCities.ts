import { City } from "../../../../country/region/department/city/City"
import { nouvelleCaledonieCities } from "./988/NouvelleCaledonieCities"

export const collectiviteOutreMerCities: City[] = [
  ...nouvelleCaledonieCities
]
