import { FranceDepartementCode } from "../FranceDepartementCode"

export enum CollectiviteOutreMerDepartementCode {
  NouvelleCaledonie = FranceDepartementCode.NouvelleCaledonie,
}
