import { Department } from "../../../../country/region/department/Department"
import { nouvelleCaledonie } from "./988/NouvelleCaledonie"

export const collectiviteOutreMerDepartments: Department[] = [
  nouvelleCaledonie
]
