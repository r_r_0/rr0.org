import { NouvelleCaledonieCityCode } from "./NouvelleCaledonieCityCode"
import { OrganizationMessages } from "../../../../../OrganizationMessages"

export type NouvelleCaledonieCityMessages = { [key in NouvelleCaledonieCityCode]: OrganizationMessages }
