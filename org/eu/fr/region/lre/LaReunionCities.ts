import { City } from "../../../../country/region/department/city/City"
import { laReunion974Cities } from "./974/LaReunionCities"

export const laReunionCities: City[] = [
  ...laReunion974Cities
]
