import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const laReunion = franceRegion(FranceRegionCode.lre, Place.fromDMS("21°06′52″S,55°31′57″E"))
