import { FranceDepartementCode } from "../FranceDepartementCode"

export enum LaReunionDepartementCode {
  LaReunion = FranceDepartementCode.LaReunion,
}
