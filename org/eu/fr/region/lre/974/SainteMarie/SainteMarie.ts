import { Place } from "../../../../../../../place/Place"
import { LaReunionCityCode } from "../LaReunionCityCode"
import { City } from "../../../../../../country/region/department/city/City"
import { laReunion974 } from "../LaReunion"

export const stMarie974 = City.create(LaReunionCityCode.SteMarie, laReunion974, Place.fromDMS("20°53′49″S,55°32′57″E"))
