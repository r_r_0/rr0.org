import { Place } from "../../../../../../../place/Place"
import { LaReunionCityCode } from "../LaReunionCityCode"
import { City } from "../../../../../../country/region/department/city/City"
import { laReunion974 } from "../LaReunion"

export const stBenoit974 = City.create(LaReunionCityCode.StBenoit, laReunion974, Place.fromDMS("21°02′02″S,55°42′46″E"))
