import { Place } from "../../../../../../../place/Place"
import { LaReunionCityCode } from "../LaReunionCityCode"
import { City } from "../../../../../../country/region/department/city/City"
import { laReunion974 } from "../LaReunion"

export const saintDenis974 = City.create(LaReunionCityCode.SaintDenis, laReunion974,
  Place.fromDMS("20°52′44″S,55°26′53″E"))
