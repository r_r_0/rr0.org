import { LaReunionCityCode } from "./LaReunionCityCode"
import { OrganizationMessages } from "../../../../../OrganizationMessages"

export type LaReunionMessages = { [key in LaReunionCityCode]: OrganizationMessages }
