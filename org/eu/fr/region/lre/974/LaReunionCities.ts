import { City } from "../../../../../country/region/department/city/City"
import { stMarie974 } from "./SainteMarie/SainteMarie"
import { stBenoit974 } from "./SaintBenoit/SaintBenoit"
import { saintPierre974 } from "./SaintPierre/SaintPierre"
import { saintDenis974 } from "./SaintDenis/SaintDenis"
import { saintPaul974 } from "./SaintPaul/SaintPaul"

export const laReunion974Cities: City[] = [
  stBenoit974,
  saintDenis974,
  stMarie974,
  saintPaul974,
  saintPierre974
]
