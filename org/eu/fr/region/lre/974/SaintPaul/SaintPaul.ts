import { Place } from "../../../../../../../place/Place"
import { LaReunionCityCode } from "../LaReunionCityCode"
import { City } from "../../../../../../country/region/department/city/City"
import { laReunion974 } from "../LaReunion"

export const saintPaul974 = City.create(LaReunionCityCode.SaintPaul, laReunion974,
  Place.fromDMS("21° 00′ 35″ S, 55° 16′ 11″E"))
