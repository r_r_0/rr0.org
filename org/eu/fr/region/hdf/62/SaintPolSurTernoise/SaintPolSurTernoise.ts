import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PasDeCalaisCityCode } from "../PasDeCalaisCityCode"

export const saintPolSurTernoise = franceCity(PasDeCalaisCityCode.SaintPolSurTernoise,
  Place.fromDMS("50°22′47″N,2°20′08″E"))
