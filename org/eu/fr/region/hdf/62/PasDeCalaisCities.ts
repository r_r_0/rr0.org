import { saintPolSurTernoise } from "./SaintPolSurTernoise/SaintPolSurTernoise"
import { City } from "../../../../../country/region/department/city/City"
import { calais } from "./Calais/Calais"
import { neuvilleSaintVaast } from "./NeuvilleSaintVaast/NeuvilleSaintVaast"

export const pasDeCalaisCities: City[] = [
  calais,
  neuvilleSaintVaast,
  saintPolSurTernoise
]
