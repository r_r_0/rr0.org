import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PasDeCalaisCityCode } from "../PasDeCalaisCityCode"

export const neuvilleSaintVaast = franceCity(PasDeCalaisCityCode.NeuvilleSaintVaast,
  Place.fromDMS("50°21′22″N,2°45′32″E"))
