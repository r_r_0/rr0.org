import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PasDeCalaisCityCode } from "../PasDeCalaisCityCode"

export const calais = franceCity(PasDeCalaisCityCode.Calais, Place.fromDMS("50°56′53″N,1°51′23″E"))
