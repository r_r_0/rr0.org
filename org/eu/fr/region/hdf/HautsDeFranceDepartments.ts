import { nord } from "./59/Nord"
import { pasDeCalais } from "./62/PasDeCalais"
import { Department } from "../../../../country/region/department/Department"
import { somme } from "./80/Somme"
import { aisne } from "./02/Aisne"

export const hautsDeFranceDepartments: Department[] = [
  aisne,
  pasDeCalais,
  nord,
  somme
]
