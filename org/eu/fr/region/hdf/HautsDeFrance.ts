import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const hautsDeFrance = franceRegion(FranceRegionCode.hdf, Place.fromDMS("49°55′14″N,2°42′11″E"))
