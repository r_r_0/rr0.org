import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SommeCityCode } from "../SommeCityCode"

export const bernaville = franceCity(SommeCityCode.Bernaville, Place.fromDMS("50°07′56″N,2°09′52″E"))
