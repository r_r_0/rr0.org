import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { NordCityCode } from "../NordCityCode"

export const avesnesLesAubert = franceCity(NordCityCode.AvesnesLesAubert, Place.fromDMS("50°11′52″N,3°22′51″E"))
