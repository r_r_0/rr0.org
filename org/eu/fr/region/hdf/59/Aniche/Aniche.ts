import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { NordCityCode } from "../NordCityCode"

export const aniche59 = franceCity(NordCityCode.Aniche, Place.fromDMS("50°19′50″N,3°15′07″E"))
