import { aniche59 } from "./Aniche/Aniche"
import { City } from "../../../../../country/region/department/city/City"
import { thiant } from "./Thiant/Thiant"
import { jeumont } from "./Jeumont/Jeumont"
import { avesnesLesAubert } from "./AvesnesLesAubert/AvesnesLesAubert"

export const nordCities: City[] = [
  aniche59,
  avesnesLesAubert,
  jeumont,
  thiant
]
