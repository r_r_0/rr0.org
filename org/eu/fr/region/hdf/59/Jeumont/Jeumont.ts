import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { NordCityCode } from "../NordCityCode"

export const jeumont = franceCity(NordCityCode.Jeumont, Place.fromDMS("50°17′43″N,4°06′07″E"))
