import { sainsRichaumont } from "./SainsRichaumont/SainsRichaumont"
import { City } from "../../../../../country/region/department/city/City"

export const aisneCities: City[] = [
  sainsRichaumont
]
