import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AisneCityCode } from "../AisneCityCode"

export const sainsRichaumont = franceCity(AisneCityCode.SainsRichaumont, Place.fromDMS("49°49′29″N,3°42′36″E"))
