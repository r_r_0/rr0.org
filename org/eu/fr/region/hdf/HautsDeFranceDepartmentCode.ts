import { FranceDepartementCode } from "../FranceDepartementCode"

export enum HautsDeFranceDepartmentCode {
  Aisne = FranceDepartementCode.Aisne,
  Nord = FranceDepartementCode.Nord,
  PasDeCalais = FranceDepartementCode.PasDeCalais,
  Somme = FranceDepartementCode.Somme,
}
