import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { ParisCityCode } from "../ParisCityCode"

export const paris75 = franceCity(ParisCityCode.Paris, Place.fromDMS("48° 51′ 24″N, 2° 21′ 07″E"))
