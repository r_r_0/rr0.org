import { hautsDeSeine } from "./92/HautsDeSeine"
import { oise } from "./60/Oise"
import { paris } from "./75/Paris"
import { yvelines } from "./78/Yvelines"
import { Organization } from "../../../../Organization"
import { seineEtMarne } from "./77/SeineEtMarne"
import { valDOise } from "./95/ValDOise"
import { essonne } from "./91/Essonne"

export const idfDepartments: Organization[] = [
  essonne,
  hautsDeSeine,
  oise,
  paris,
  seineEtMarne,
  valDOise,
  yvelines
]
