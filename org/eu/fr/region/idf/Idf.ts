import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const ileDeFrance = franceRegion(FranceRegionCode.idf, Place.fromLocation(48.852222, 2.3175))
