import { City } from "../../../../../country/region/department/city/City"
import { neauphleLeChateau } from "./neauphlelechateau/NeauphleLeChateau"
import { mesnilLeRoi } from "./mesnilleroi/MesnilLeRoi"
import { noisyLeRoi } from "./noisyleroi/NoisyLeRoi"

export const yvelinesCities: City[] = [
  neauphleLeChateau,
  noisyLeRoi,
  mesnilLeRoi
]
