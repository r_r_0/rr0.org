import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YvelinesCityCode } from "../YvelinesCityCode"

export const mesnilLeRoi = franceCity(YvelinesCityCode.MesnilLeRoi, Place.fromDMS("48°56′15″N,2°07′39″E"))
