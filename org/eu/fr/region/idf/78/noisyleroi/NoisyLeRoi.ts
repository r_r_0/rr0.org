import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YvelinesCityCode } from "../YvelinesCityCode"

export const noisyLeRoi = franceCity(YvelinesCityCode.NoisyLeRoi, Place.fromDMS("48°50′49″N,2°03′39″E"))
