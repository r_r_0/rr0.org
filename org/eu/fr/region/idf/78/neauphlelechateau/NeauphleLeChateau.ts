import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YvelinesCityCode } from "../YvelinesCityCode"

export const neauphleLeChateau = franceCity(YvelinesCityCode.NeauphleLeChateau, Place.fromDMS("48°51′24″N,2°21′07″E"))
