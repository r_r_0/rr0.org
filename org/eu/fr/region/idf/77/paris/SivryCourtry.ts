import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SeineEtMarneCityCode } from "../SeineEtMarneCityCode"

export const sivryCourtry = franceCity(SeineEtMarneCityCode.SivryCourtry, Place.fromDMS("48°31′43″N,2°45′21″E"))
