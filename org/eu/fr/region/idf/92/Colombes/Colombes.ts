import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HautsDeSeineCityCode } from "../HautsDeSeineCityCode"

export const colombes = franceCity(HautsDeSeineCityCode.Colombes, Place.fromDMS("48° 55′ 25″ N, 2° 15′ 08″E"))
