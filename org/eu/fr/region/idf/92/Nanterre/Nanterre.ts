import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HautsDeSeineCityCode } from "../HautsDeSeineCityCode"

export const nanterre92 = franceCity(HautsDeSeineCityCode.Nanterre, Place.fromDMS("48° 53′ 31″N, 2° 12′ 26″E"))
