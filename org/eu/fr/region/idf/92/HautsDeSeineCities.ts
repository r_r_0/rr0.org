import { nanterre92 } from "./Nanterre/Nanterre"
import { City } from "../../../../../country/region/department/city/City"
import { colombes } from "./Colombes/Colombes"
import { boisColombes } from "./BoisColombes/BoisColombes"

export const hautsDeSeineCities: City[] = [
  boisColombes,
  colombes,
  nanterre92
]
