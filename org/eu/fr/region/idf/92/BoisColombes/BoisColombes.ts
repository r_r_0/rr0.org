import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HautsDeSeineCityCode } from "../HautsDeSeineCityCode"

export const boisColombes = franceCity(HautsDeSeineCityCode.BoisColombes, Place.fromDMS("48° 55′ 03″N,2°16′06″E"))
