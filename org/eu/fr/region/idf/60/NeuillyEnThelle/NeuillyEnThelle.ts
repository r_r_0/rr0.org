import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { OiseCityCode } from "../OiseCityCode"

export const neuillyEnThelle = franceCity(OiseCityCode.NeuillyEnThelle, Place.fromDMS("49°13′28″N,2°17′10″E"))
