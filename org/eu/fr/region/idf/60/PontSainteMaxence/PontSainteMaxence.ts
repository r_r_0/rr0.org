import { Place } from "../../../../../../../place/Place"
import { OiseCityCode } from "../OiseCityCode"
import { franceCity } from "../../../FranceCity"

export const pontSainteMaxence = franceCity(OiseCityCode.PontSainteMaxence,
  Place.fromDMS("49° 18′ 07″ N, 2° 36′ 16″ E"))
