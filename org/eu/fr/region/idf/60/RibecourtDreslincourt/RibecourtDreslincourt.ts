import { Place } from "../../../../../../../place/Place"
import { OiseCityCode } from "../OiseCityCode"
import { franceCity } from "../../../FranceCity"

export const ribecourtDreslincourt = franceCity(OiseCityCode.RibecourtDreslincourt,
  Place.fromDMS("49°30′39″N,2°55′24″E"))
