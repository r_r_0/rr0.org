import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { OiseCityCode } from "../OiseCityCode"

export const compiegne = franceCity(OiseCityCode.Compiegne, Place.fromDMS("49°24′54″N,2°49′23″E"))
