import { City } from "../../../../../country/region/department/city/City"
import { pontLEveque60 } from "./PontLEveque/PontLEveque"
import { compiegne } from "./compiegne/Compiegne"
import { neuillyEnThelle } from "./NeuillyEnThelle/NeuillyEnThelle"
import { ribecourtDreslincourt } from "./RibecourtDreslincourt/RibecourtDreslincourt"
import { morlincourt } from "./morlincourt/Morlincourt"
import { pontSainteMaxence } from "./PontSainteMaxence/PontSainteMaxence"

export const oiseCities: City[] = [
  compiegne,
  morlincourt,
  neuillyEnThelle,
  pontLEveque60,
  pontSainteMaxence,
  ribecourtDreslincourt
]
