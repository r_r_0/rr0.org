import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { OiseCityCode } from "../OiseCityCode"

export const morlincourt = franceCity(OiseCityCode.Morlincourt, Place.fromDMS("49°34′14″N,3°02′14″E"))
