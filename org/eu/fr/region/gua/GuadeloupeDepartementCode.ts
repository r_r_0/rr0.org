import { FranceDepartementCode } from "../FranceDepartementCode"

export enum GuadeloupeDepartementCode {
  Guadeloupe = FranceDepartementCode.Guadeloupe,
}
