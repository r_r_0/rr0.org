import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const guadeloupeRegion = franceRegion(FranceRegionCode.gua, Place.fromDMS("16°15′34″N,61°33′38″O"))
