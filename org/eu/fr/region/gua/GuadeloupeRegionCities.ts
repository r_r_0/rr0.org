import { City } from "../../../../country/region/department/city/City"
import { guadeloupeDepCities } from "./971/GuadeloupeCities"

export const guadeloupeRegionCities: City[] = [
  ...guadeloupeDepCities
]
