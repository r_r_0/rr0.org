import { GuadeloupeCityCode } from "./GuadeloupeCityCode"
import { OrganizationMessages } from "../../../../../OrganizationMessages"

export type GuadeloupeCityMessage = { [key in GuadeloupeCityCode]: OrganizationMessages }
