import { City } from "../../../../../country/region/department/city/City"
import { capesterreMarieGalante } from "./CapesterreMarieGalante/CapesterreMarieGalante"

export const guadeloupeDepCities: City[] = [
  capesterreMarieGalante
]
