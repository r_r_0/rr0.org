import { Department } from "../../../../country/region/department/Department"
import { guadeloupe } from "./971/Guadeloupe"

export const guadeloupeDepartments: Department[] = [
  guadeloupe
]
