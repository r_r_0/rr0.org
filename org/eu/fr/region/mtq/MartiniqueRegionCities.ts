import { City } from "../../../../country/region/department/city/City"
import { martinique972Cities } from "./972/MartiniqueCities"

export const martiniqueRegionCities: City[] = [
  ...martinique972Cities
]
