import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const martiniqueRegion = franceRegion(FranceRegionCode.mtq, Place.fromDMS("14°39′00″N,61°00′54″W"))
