import { FranceDepartementCode } from "../FranceDepartementCode"

export enum MartiniqueDepartementCode {
  Martinique = FranceDepartementCode.Martinique,
}
