import { FranceDepartementCode } from "../FranceDepartementCode"

export enum PaysDeLoireDepartementCode {
  Sarthe = FranceDepartementCode.Sarthe,
  LoireAtlantique = FranceDepartementCode.LoireAtlantique,
  MaineEtLoire = FranceDepartementCode.MaineEtLoire,
  Mayenne = FranceDepartementCode.Mayenne
}
