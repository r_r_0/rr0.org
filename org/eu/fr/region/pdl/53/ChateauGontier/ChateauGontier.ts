import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MayenneCityCode } from "../MayenneCityCode"

export const chateauGontier = franceCity(MayenneCityCode.ChateauGontier,
  Place.fromDMS("47°14′20″N,1°20′52″O"))
