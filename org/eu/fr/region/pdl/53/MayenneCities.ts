import { City } from "../../../../../country/region/department/city/City"
import { chateauGontier } from "./ChateauGontier/ChateauGontier"

export const mayenneCities: City[] = [
  chateauGontier
]
