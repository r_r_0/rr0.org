import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SartheCityCode } from "../SartheCityCode"

export const steCerotte = franceCity(SartheCityCode.SteCerotte, Place.fromDMS("47°54′02″N,0°41′15″E"))
