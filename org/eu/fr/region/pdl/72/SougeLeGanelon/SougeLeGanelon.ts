import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SartheCityCode } from "../SartheCityCode"

export const sougeLeGanelon = franceCity(SartheCityCode.SougeLeGanelon, Place.fromDMS("8°19′05″N,0°01′50″W"))
