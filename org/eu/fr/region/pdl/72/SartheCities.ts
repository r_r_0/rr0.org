import { City } from "../../../../../country/region/department/city/City"
import { leMans72 } from "./LeMans/LeMans"
import { steCerotte } from "./SteCerotte/SteCerotte"
import { bazoge } from "./bazoge/Bazoge"
import { sougeLeGanelon } from "./SougeLeGanelon/SougeLeGanelon"

export const sartheCities: City[] = [
  bazoge,
  leMans72,
  steCerotte,
  sougeLeGanelon
]
