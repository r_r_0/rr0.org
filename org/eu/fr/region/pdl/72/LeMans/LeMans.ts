import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SartheCityCode } from "../SartheCityCode"

export const leMans72 = franceCity(SartheCityCode.LeMans, Place.fromLocation(48.004167, 0.196944))
