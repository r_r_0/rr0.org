import { sarthe } from "./72/Sarthe"
import { maineEtLoire } from "./49/MaineEtLoire"
import { loireAtlantique } from "./44/LoireAtlantique"
import { Department } from "../../../../country/region/department/Department"
import { mayenne } from "./53/Mayenne"

export const paysDeLoireDepartments: Department[] = [
  loireAtlantique,
  maineEtLoire,
  mayenne,
  sarthe
]
