import { City } from "../../../../../country/region/department/city/City"
import { leCroisic } from "./LeCroisic/LeCroisic"
import { lorouxBottereau } from "./LorouxBottereau/LorouxBottereau"
import { reze } from "./Reze/Reze"

export const loireAtlantiqueCities: City[] = [
  leCroisic,
  lorouxBottereau,
  reze
]
