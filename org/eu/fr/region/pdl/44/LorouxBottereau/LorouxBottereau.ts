import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoireAtlantiqueCityCode } from "../LoireAtlantiqueCityCode"

export const lorouxBottereau = franceCity(LoireAtlantiqueCityCode.LorouxBottereau,
  Place.fromDMS("47°14′20″N,1°20′52″O"))
