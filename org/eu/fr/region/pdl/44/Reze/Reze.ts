import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoireAtlantiqueCityCode } from "../LoireAtlantiqueCityCode"

export const reze = franceCity(LoireAtlantiqueCityCode.Reze, Place.fromDMS("47° 10′ 38″N,1°32′57″W"))
