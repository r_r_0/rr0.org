import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoireAtlantiqueCityCode } from "../LoireAtlantiqueCityCode"

export const leCroisic = franceCity(LoireAtlantiqueCityCode.LeCroisic, Place.fromDMS("47°17′33″N,2°31′15″O"))
