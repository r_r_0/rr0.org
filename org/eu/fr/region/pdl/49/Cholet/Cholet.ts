import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MaineEtLoireCityCode } from "../MaineEtLoireCityCode"

export const cholet = franceCity(MaineEtLoireCityCode.Cholet, Place.fromDMS("47° 03′ 36″N, 0° 52′ 42″W"))
