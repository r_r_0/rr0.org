import { City } from "../../../../../country/region/department/city/City"
import { angers } from "./Angers/Angers"
import { cholet } from "./Cholet/Cholet"

export const maineEtLoireCities: City[] = [
  angers,
  cholet
]
