import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MaineEtLoireCityCode } from "../MaineEtLoireCityCode"

export const angers = franceCity(MaineEtLoireCityCode.Angers, Place.fromDMS("47°28′25″N,0°33′15″O"))
