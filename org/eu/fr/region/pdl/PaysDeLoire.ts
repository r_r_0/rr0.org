import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const paysDeLoire = franceRegion(FranceRegionCode.pdl, Place.fromLocation(47.466667, 0.833333))
