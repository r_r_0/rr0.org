import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CantalCityCode } from "../CantalCityCode"

export const mauriac = franceCity(CantalCityCode.Mauriac, Place.fromDMS("45°13′11″N,2°20′03″E"))
