import { mauriac } from "./mauriac/Mauriac"
import { City } from "../../../../../country/region/department/city/City"
import { pierrefort } from "./pierrefort/Pierrefort"

export const cantalCities: City[] = [
  mauriac,
  pierrefort
]
