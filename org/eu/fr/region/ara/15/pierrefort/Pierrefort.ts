import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CantalCityCode } from "../CantalCityCode"

export const pierrefort = franceCity(CantalCityCode.Pierrefort, Place.fromDMS("44°55′21″N,2°50′19″E"))
