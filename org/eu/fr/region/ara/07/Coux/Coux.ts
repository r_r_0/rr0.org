import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { ArdecheCityCode } from "../ArdecheCityCode"

export const coux = franceCity(ArdecheCityCode.Coux, Place.fromDMS("44°44′06″N,4°37′16″E"))
