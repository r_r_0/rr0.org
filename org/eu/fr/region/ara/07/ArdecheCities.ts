import { coux } from "./Coux/Coux"
import { City } from "../../../../../country/region/department/city/City"
import { largentiere } from "./Largentiere/Largentiere"
import { vernouxEnVivarais } from "./VernouxEnVivarais/VernouxEnVivarais"

export const ardecheCities: City[] = [
  coux,
  largentiere,
  vernouxEnVivarais
]
