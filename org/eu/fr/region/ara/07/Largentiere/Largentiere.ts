import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { ArdecheCityCode } from "../ArdecheCityCode"

export const largentiere = franceCity(ArdecheCityCode.Largentiere, Place.fromDMS("44°32′37″N,4°17′39″E"))
