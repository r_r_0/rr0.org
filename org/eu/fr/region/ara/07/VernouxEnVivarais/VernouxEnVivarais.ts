import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { ArdecheCityCode } from "../ArdecheCityCode"

export const vernouxEnVivarais = franceCity(ArdecheCityCode.VernouxEnVivarais, Place.fromDMS("44°53′47″N,4°38′46″E"))
