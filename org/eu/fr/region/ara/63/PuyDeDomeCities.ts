import { riom63 } from "./Riom/Riom"
import { City } from "../../../../../country/region/department/city/City"
import { billom } from "./Billom/Billom"
import { chatelGuyon } from "./ChatelGuyon/ChatelGuyon"

export const puyDeDomeCities: City[] = [
  riom63,
  billom,
  chatelGuyon
]
