import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PuyDeDomeCityCode } from "../PuyDeDomeCityCode"

export const billom = franceCity(PuyDeDomeCityCode.Billom, Place.fromDMS("45°53′40″N,3°06′48″E"))
