import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { PuyDeDomeCityCode } from "../PuyDeDomeCityCode"

export const chatelGuyon = franceCity(PuyDeDomeCityCode.ChatelGuyon, Place.fromDMS("45° 55′ 24″N, 3° 03′ 54″E"))
