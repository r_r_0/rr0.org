import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoireCityCode } from "../LoireCityCode"

export const roanne = franceCity(LoireCityCode.Roanne, Place.fromDMS("46°02′12″N,4°04′08″E"))
