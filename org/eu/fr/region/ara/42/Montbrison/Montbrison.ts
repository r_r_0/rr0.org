import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoireCityCode } from "../LoireCityCode"

export const montbrison42 = franceCity(LoireCityCode.Montbrison, Place.fromDMS("45° 36′ 30″ N, 4° 03′ 57″E"))
