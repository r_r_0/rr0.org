import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { LoireCityCode } from "../LoireCityCode"

export const stEtienne42 = franceCity(LoireCityCode.StEtienne, Place.fromDMS("45°26′05″N,4°23′25″E"))
