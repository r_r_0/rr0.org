import { stEtienne42 } from "./SaintEtienne/SaintEtienne"
import { roanne } from "./Roanne/Roanne"
import { City } from "../../../../../country/region/department/city/City"
import { montbrison42 } from "./Montbrison/Montbrison"

export const loireCities: City[] = [
  montbrison42,
  roanne,
  stEtienne42
]
