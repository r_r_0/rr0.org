import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const auvergneRhoneAlpes = franceRegion(FranceRegionCode.ara, Place.fromLocation(45.833333, 4.666667))
