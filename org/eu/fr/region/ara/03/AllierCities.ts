import { vallonEnSully } from "./valonensully/VallonEnSully"
import { City } from "../../../../../country/region/department/city/City"
import { montmarault } from "./montmarault/Montmarault"

export const allierCities: City[] = [
  montmarault,
  vallonEnSully
]
