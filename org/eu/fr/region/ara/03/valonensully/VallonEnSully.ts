import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AllierCityCode } from "../AllierCityCode"

export const vallonEnSully = franceCity(AllierCityCode.VallonEnSully, Place.fromDMS("46°32′13″N,2°36′32″E"))
