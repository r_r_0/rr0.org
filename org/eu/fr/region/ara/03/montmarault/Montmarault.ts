import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AllierCityCode } from "../AllierCityCode"

export const montmarault = franceCity(AllierCityCode.Montmarault, Place.fromDMS("46°19′06″N,2°57′20″E"))
