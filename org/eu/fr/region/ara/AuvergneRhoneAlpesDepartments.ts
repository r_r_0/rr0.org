import { loire } from "./42/Loire"
import { rhone } from "./69/Rhone"
import { puyDeDome } from "./63/PuyDeDome"
import { allier } from "./03/Allier"
import { isere } from "./38/Isere"
import { drome } from "./26/Drome"
import { ardeche } from "./07/Ardeche"
import { Department } from "../../../../country/region/department/Department"
import { cantal } from "./15/Cantal"
import { ain } from "./01/Ain"
import { savoie } from "./73/Savoie"

export const auvergneRhoneAlpesDepartments: Department[] = [
  ain,
  allier,
  ardeche,
  cantal,
  drome,
  isere,
  loire,
  rhone,
  puyDeDome,
  savoie
]
