import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { RhoneCityCode } from "../RhoneCityCode"

export const belleville69 = franceCity(RhoneCityCode.Belleville, Place.fromDMS("46° 06′ 34″ N, 4° 45′ 00″ E"))
