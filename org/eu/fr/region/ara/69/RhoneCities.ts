import { lyon69 } from "./Lyon/Lyon"
import { belleville69 } from "./Belleville/Belleville"
import { Organization } from "../../../../../Organization"

export const rhoneCities: Organization[] = [
  lyon69,
  belleville69
]
