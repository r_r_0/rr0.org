import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const saintVerand = franceCity(IsereCityCode.SaintVerand, Place.fromDMS("45° 10′ 26″ N, 5° 19′ 57″E"))
