import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const allevard = franceCity(IsereCityCode.Allevard, Place.fromDMS("45° 23′ 40″N, 6° 04′ 29″E"))
