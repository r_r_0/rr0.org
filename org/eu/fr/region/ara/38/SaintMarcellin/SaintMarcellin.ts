import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const saintMarcellin = franceCity(IsereCityCode.SaintMarcellin, Place.fromDMS("45°09′14″N,5°19′14″E"))
