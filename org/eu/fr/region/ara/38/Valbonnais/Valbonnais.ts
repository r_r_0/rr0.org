import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const valbonnais = franceCity(IsereCityCode.Valbonnais, Place.fromDMS("44°54′03″N,5°54′18″E"))
