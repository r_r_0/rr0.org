import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const domene = franceCity(IsereCityCode.Domene, Place.fromDMS("45°12′12″N,5°50′23″E"))
