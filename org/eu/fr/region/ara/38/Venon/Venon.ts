import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const venon = franceCity(IsereCityCode.Venon, Place.fromDMS("45° 10′ 22″N, 5° 48′ 19″E"))
