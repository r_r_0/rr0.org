import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const chirens = franceCity(IsereCityCode.Chirens, Place.fromDMS("45° 24′ 50″N, 5° 33′ 21″E"))
