import { saintMarcellin } from "./SaintMarcellin/SaintMarcellin"
import { Organization } from "../../../../../Organization"
import { meylan } from "./Meylan/Meylan"
import { domene } from "./Domene/Domene"
import { valbonnais } from "./Valbonnais/Valbonnais"
import { saintGeoireEnValdaine } from "./SaintGeoireEnValdaine/SaintGeoireEnValdaine"
import { chirens } from "./Chirens/Chirens"
import { saintVerand } from "./SaintVerand/SaintVerand"
import { allevard } from "./Allevard/Allevard"
import { venon } from "./Venon/Venon"

export const isereCities: Organization[] = [
  allevard,
  chirens,
  domene,
  meylan,
  saintGeoireEnValdaine,
  saintMarcellin,
  saintVerand,
  valbonnais,
  venon
]
