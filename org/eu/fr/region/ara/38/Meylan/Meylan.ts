import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const meylan = franceCity(IsereCityCode.Meylan, Place.fromDMS("45°12′33″N,5°46′48″E"))
