import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { IsereCityCode } from "../IsereCityCode"

export const saintGeoireEnValdaine = franceCity(IsereCityCode.SaintGeoireEnValdaine,
  Place.fromDMS(`45°27′27″N,5°38′08″E`))
