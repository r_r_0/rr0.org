import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { DromeCityCode } from "../DromeCityCode"

export const chateaudouble = franceCity(DromeCityCode.Chateaudouble, Place.fromDMS("44°54′01″N,5°05′46″E"))
