import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SavoieCityCode } from "../SavoieCityCode"

export const saintJeanDeMaurienne = franceCity(SavoieCityCode.SaintJeanDeMaurienne,
  Place.fromDMS("45°16′22″N,6°20′54″E"))
