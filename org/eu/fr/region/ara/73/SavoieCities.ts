import { saintJeanDeMaurienne } from "./SaintJeanDeMaurienne/SaintJeanDeMaurienne"
import { City } from "../../../../../country/region/department/city/City"

export const savoieCities: City[] = [
  saintJeanDeMaurienne
]
