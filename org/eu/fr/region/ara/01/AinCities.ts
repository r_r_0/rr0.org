import { hautevilleLompnes } from "./HautevilleLompnes/HautevilleLompnes"
import { City } from "../../../../../country/region/department/city/City"

export const ainCities: City[] = [
  hautevilleLompnes
]
