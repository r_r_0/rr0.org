import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AinCityCode } from "../AinCityCode"

export const hautevilleLompnes = franceCity(AinCityCode.HautevilleLompnes, Place.fromDMS("45°58′45″N,5°36′19″E"))
