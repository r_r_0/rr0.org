import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VarCityCode } from "../VarCityCode"

export const foxAmphoux = franceCity(VarCityCode.FoxAmphoux, Place.fromDMS("43°35′52″N,6°05′37″E"))
