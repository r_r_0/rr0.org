import { City } from "../../../../../country/region/department/city/City"
import { foxAmphoux } from "./FoxAmphoux/FoxAmphoux"
import { carces } from "./Carces/Carces"
import { grimaud } from "./Grimaud/Grimaud"

export const varCities: City[] = [
  carces,
  foxAmphoux,
  grimaud
]
