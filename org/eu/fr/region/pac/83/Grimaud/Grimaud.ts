import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VarCityCode } from "../VarCityCode"

export const grimaud = franceCity(VarCityCode.Grimaud, Place.fromDMS("43°16′27″N,6°31′20″E"))
