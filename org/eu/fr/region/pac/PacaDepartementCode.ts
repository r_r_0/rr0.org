import { FranceDepartementCode } from "../FranceDepartementCode"

export enum PacaDepartementCode {
  AlpesDeHauteProvence = FranceDepartementCode.AlpesDeHauteProvence,
  AlpesMaritimes = FranceDepartementCode.AlpesMaritimes,
  BouchesDuRhone = FranceDepartementCode.BouchesDuRhone,
  HautesAlpes = FranceDepartementCode.HautesAlpes,
  Var = FranceDepartementCode.Var,
  Vaucluse = FranceDepartementCode.Vaucluse,
  Vendee = FranceDepartementCode.Vendee,
}
