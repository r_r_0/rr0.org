import { cannes06 } from "./cannes/Cannes"
import { City } from "../../../../../country/region/department/city/City"
import { cagnesSurMer } from "./cagnessurmer/CagnesSurMer"

export const alpesMaritimesCities: City[] = [
  cannes06,
  cagnesSurMer
]
