import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AlpesMaritimesCityCode } from "../AlpesMaritimesCityCode"

export const cannes06 = franceCity(AlpesMaritimesCityCode.Cannes, Place.fromDMS("43° 33′ 05″N, 7° 00′ 46″E"))
