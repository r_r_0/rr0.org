import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AlpesMaritimesCityCode } from "../AlpesMaritimesCityCode"

export const cagnesSurMer = franceCity(AlpesMaritimesCityCode.CagnesSurMer, Place.fromDMS("43°39′52″N,7°08′56″E"))
