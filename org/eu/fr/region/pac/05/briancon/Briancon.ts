import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HautesAlpesCityCode } from "../HautesAlpesCityCode"

export const briancon05 = franceCity(HautesAlpesCityCode.Briancon, Place.fromLocation(44.896389, 6.635556))
