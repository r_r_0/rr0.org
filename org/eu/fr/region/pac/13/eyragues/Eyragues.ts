import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { BouchesDuRhoneCityCode } from "../BouchesDuRhoneCityCode"

export const eyragues = franceCity(BouchesDuRhoneCityCode.Eyragues, Place.fromDMS("43°50′31″N,4°50′30″E"))
