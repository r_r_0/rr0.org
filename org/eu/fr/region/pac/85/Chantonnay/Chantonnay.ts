import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VendeeCityCode } from "../VendeeCityCode"

export const chantonnay = franceCity(VendeeCityCode.Chantonnay, Place.fromDMS("46°41′16″N,1°02′58″W"))
