import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VaucluseCityCode } from "../VaucluseCityCode"

export const bollene = franceCity(VaucluseCityCode.Bollene, Place.fromDMS("44°16′52″N,4°44′58″E"))
