import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AlpesDeHauteProvenceCityCode } from "../AlpesDeHauteProvenceCityCode"

export const barcelonnette = franceCity(AlpesDeHauteProvenceCityCode.Barcelonnette,
  Place.fromDMS("44°23′12″N,6°39′11″E"))
