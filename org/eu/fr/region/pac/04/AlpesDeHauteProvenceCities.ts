import { barcelonnette } from "./Barcelonnette/Barcelonnette"
import { City } from "../../../../../country/region/department/city/City"
import { lauzetUbaye } from "./LauzetUbaye/LauzetUbaye"
import { entrevaux } from "./Entrevaux/Entrevaux"

export const alpesDeHauteProvenceCities: City[] = [
  barcelonnette,
  entrevaux,
  lauzetUbaye
]
