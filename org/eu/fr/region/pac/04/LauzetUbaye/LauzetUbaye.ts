import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { AlpesDeHauteProvenceCityCode } from "../AlpesDeHauteProvenceCityCode"

export const lauzetUbaye = franceCity(AlpesDeHauteProvenceCityCode.LauzetUbaye,
  Place.fromDMS("44°25′51″N,6°25′58″E"))
