import { meurtheEtMoselle } from "./54/MeurtheEtMoselle"
import { marne } from "./51/Marne"
import { meuse } from "./55/Meuse"
import { basRhin } from "./67/BasRhin"
import { moselle } from "./57/Moselle"
import { vosges } from "./88/Vosges"
import { ardennes } from "./08/Ardennes"
import { Department } from "../../../../country/region/department/Department"
import { hautRhin } from "./68/HautRhin"

export const grandEstDepartments: Department[] = [
  ardennes,
  basRhin,
  hautRhin,
  marne,
  meurtheEtMoselle,
  meuse,
  moselle,
  vosges
]
