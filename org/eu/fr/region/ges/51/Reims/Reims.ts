import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MarneCityCode } from "../MarneCityCode"

export const reims = franceCity(MarneCityCode.Reims, Place.fromDMS("49°15′46″N,4°02′05″E"))
