import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { HautRhinCityCode } from "../HautRhinCityCode"

export const mulhouse = franceCity(HautRhinCityCode.Mulhouse, Place.fromDMS("47° 44′ 58″ N, 7° 20′ 24″E"))
