import { City } from "../../../../../country/region/department/city/City"
import { mulhouse } from "./Mulhouse/Mulhouse"

export const hautRhinCities: City[] = [
  mulhouse
]
