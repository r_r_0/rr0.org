import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MoselleCityCode } from "../MoselleCityCode"

export const arsSurMoselle = franceCity(MoselleCityCode.ArsSurMoselle, Place.fromDMS("49°04′44″N,6°04′30″E"))
