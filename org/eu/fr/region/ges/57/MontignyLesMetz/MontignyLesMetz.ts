import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MoselleCityCode } from "../MoselleCityCode"

export const montignyLesMetz = franceCity(MoselleCityCode.MontignyLesMetz, Place.fromDMS("49° 06′ 02″N,6°09′14″E"))
