import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MoselleCityCode } from "../MoselleCityCode"

export const bouzonville = franceCity(MoselleCityCode.Bouzonville, Place.fromDMS("49° 17′ 33″ N, 6° 32′ 06″E"))
