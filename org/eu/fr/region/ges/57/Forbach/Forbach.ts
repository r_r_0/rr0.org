import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MoselleCityCode } from "../MoselleCityCode"

export const forbach = franceCity(MoselleCityCode.Forbach, Place.fromDMS("49° 11′ 20″N, 6° 54′ 03″E"))
