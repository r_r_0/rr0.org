import { vaucouleurs } from "./Vaucouleurs/Vaucouleurs"
import { City } from "../../../../../country/region/department/city/City"
import { voidVacon } from "./VoidVacon/VoidVacon"
import { gondrecourtLeChateau } from "./GondrecourtLeChateau/GondrecourtLeChateau"

export const meuseCities: City[] = [
  gondrecourtLeChateau,
  vaucouleurs,
  voidVacon
]
