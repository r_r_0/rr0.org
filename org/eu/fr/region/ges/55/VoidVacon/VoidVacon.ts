import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MeuseCityCode } from "../MeuseCityCode"

export const voidVacon = franceCity(MeuseCityCode.VoidVacon, Place.fromDMS("48° 41′ 19″N, 5° 37′ 08″E"))
