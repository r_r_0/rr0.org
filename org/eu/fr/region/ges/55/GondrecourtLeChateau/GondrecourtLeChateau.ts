import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MeuseCityCode } from "../MeuseCityCode"

export const gondrecourtLeChateau = franceCity(MeuseCityCode.GondrecourtLeChateau,
  Place.fromDMS("48°30′51″N,5°30′28″E"))
