import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MeuseCityCode } from "../MeuseCityCode"

export const vaucouleurs = franceCity(MeuseCityCode.Vaucouleurs, Place.fromDMS("48°36′09″N,5°39′57″E"))
