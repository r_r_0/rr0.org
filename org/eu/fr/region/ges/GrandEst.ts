import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const grandEst = franceRegion(FranceRegionCode.ges, Place.fromDMS("48°35′56″N,7°45′36″E"))
