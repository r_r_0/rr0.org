import { grandEstDepartmentsMessages } from "./GrandEstDepartmentsMessages"
import { RegionMessages } from "../../../../country/region/RegionMessages"

export const grandEstMessages = RegionMessages.create("Grand Est", grandEstDepartmentsMessages)
