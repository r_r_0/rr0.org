import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VosgesCityCode } from "../VosgesCityCode"

export const anglemont = franceCity(VosgesCityCode.Anglemont, Place.fromDMS("48° 22′ 51″ N, 6° 40′ 10″E"))
