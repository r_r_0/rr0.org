import { epinal } from "./Epinal/Epinal"
import { City } from "../../../../../country/region/department/city/City"
import { anglemont } from "./Anglemont/Anglemont"

export const vosgesCities: City[] = [
  anglemont,
  epinal
]
