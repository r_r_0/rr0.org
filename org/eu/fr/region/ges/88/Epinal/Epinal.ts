import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { VosgesCityCode } from "../VosgesCityCode"

export const epinal = franceCity(VosgesCityCode.Epinal, Place.fromDMS("48° 10′ 28″ N, 6° 27′ 04″E"))
