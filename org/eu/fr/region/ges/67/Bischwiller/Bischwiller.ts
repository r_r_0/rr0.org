import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { BasRhinCityCode } from "../BasRhinCityCode"

export const bischwiller = franceCity(BasRhinCityCode.Bischwiller, Place.fromDMS("48° 46′ 04″ N, 7° 51′ 36″E"))
