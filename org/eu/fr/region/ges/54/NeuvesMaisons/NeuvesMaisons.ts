import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MeurtheEtMoselleCityCode } from "../MeurtheEtMoselleCityCode"

export const neuvesMaisons = franceCity(MeurtheEtMoselleCityCode.NeuvesMaisons, Place.fromDMS("48°37′01″N,6°06′16″E"))
