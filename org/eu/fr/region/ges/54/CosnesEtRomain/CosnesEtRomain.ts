import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MeurtheEtMoselleCityCode } from "../MeurtheEtMoselleCityCode"

export const cosnesEtRomain = franceCity(MeurtheEtMoselleCityCode.CosnesEtRomain, Place.fromDMS("49°31′12″N,5°42′43″E"))
