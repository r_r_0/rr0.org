import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MeurtheEtMoselleCityCode } from "../MeurtheEtMoselleCityCode"

export const nancy = franceCity(MeurtheEtMoselleCityCode.Nancy, Place.fromDMS("48°41′37″N,6°11′05″E"))
