import { cosnesEtRomain } from "./CosnesEtRomain/CosnesEtRomain"
import { nancy } from "./Nancy/Nancy"
import { Organization } from "../../../../../Organization"
import { neuvesMaisons } from "./NeuvesMaisons/NeuvesMaisons"

export const meurtheEtMoselleCities: Organization[] = [
  cosnesEtRomain,
  nancy,
  neuvesMaisons
]
