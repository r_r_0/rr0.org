import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const bretagne = franceRegion(FranceRegionCode.bre, Place.fromDMS("48°07′19″N,2°46′08″W"))
