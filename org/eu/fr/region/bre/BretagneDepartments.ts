import { cotesDArmor } from "./22/CotesDArmor"
import { Department } from "../../../../country/region/department/Department"
import { illeEtVilaine } from "./35/IlleEtVilaine"
import { finistere } from "./29/Finistere"

export const bretagneDepartments: Department[] = [
  cotesDArmor,
  finistere,
  illeEtVilaine
]
