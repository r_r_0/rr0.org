import { FranceDepartementCode } from "../FranceDepartementCode"

export enum BretagneDepartementCode {
  CotesDArmor = FranceDepartementCode.CotesDArmor,
  IlleEtVilaine = FranceDepartementCode.IlleEtVilaine,
  Finistere = FranceDepartementCode.Finistere,
}
