import { City } from "../../../../../country/region/department/city/City"
import { concarneau } from "./Concarneau/Concarneau"
import { fouesnant } from "./Fouesnant/Fouesnant"
import { brest } from "./Brest/Brest"

export const finistereCities: City[] = [
  brest,
  concarneau,
  fouesnant
]
