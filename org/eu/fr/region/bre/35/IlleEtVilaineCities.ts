import { City } from "../../../../../country/region/department/city/City"
import { pleineFougeres } from "./pleinefougeres/PleineFougeres"

export const illeEtVilaineCities: City[] = [
  pleineFougeres
]
