import { Place } from "../../../../../../../place/Place"
import { IlleEtVilaineCityCode } from "../IlleEtVilaineCityCode"
import { franceCity } from "../../../FranceCity"

export const pleineFougeres = franceCity(IlleEtVilaineCityCode.PleineFougeres, Place.fromDMS("48°32′01″N,1°33′51″O"))
