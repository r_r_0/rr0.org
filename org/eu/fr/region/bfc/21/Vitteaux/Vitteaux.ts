import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CoteDOrCityCode } from "../CoteDOrCityCode"

export const vitteaux = franceCity(CoteDOrCityCode.Vitteaux, Place.fromDMS("47°23′55″N,4°32′33″E"))
