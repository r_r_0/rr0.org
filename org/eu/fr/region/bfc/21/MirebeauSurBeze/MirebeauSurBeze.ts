import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { CoteDOrCityCode } from "../CoteDOrCityCode"

export const mirebeauSurBeze = franceCity(CoteDOrCityCode.MirebeauSurBeze, Place.fromDMS("47°23′59″N,5°19′09″E"))
