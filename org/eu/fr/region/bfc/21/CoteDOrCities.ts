import { City } from "../../../../../country/region/department/city/City"
import { vitteaux } from "./Vitteaux/Vitteaux"
import { mirebeauSurBeze } from "./MirebeauSurBeze/MirebeauSurBeze"

export const coteDOrCities: City[] = [
  mirebeauSurBeze,
  vitteaux
]
