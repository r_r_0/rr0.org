import { franceRegion } from "../FranceRegion"
import { FranceRegionCode } from "../FranceRegionCode"
import { Place } from "../../../../../place/Place"

export const bourgogneFrancheComte = franceRegion(FranceRegionCode.bfc, Place.fromDMS("47° 14′ 04″ N, 6° 01′ 50″ E"))
