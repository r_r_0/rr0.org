import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { NievreCityCode } from "../NievreCityCode"

export const chateauneufValDeBargis = franceCity(NievreCityCode.ChateauneufValDeBargis,
  Place.fromDMS(`47°17′03″N,3°13′38″E`))
