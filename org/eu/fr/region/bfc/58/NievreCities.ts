import { City } from "../../../../../country/region/department/city/City"
import { chateauneufValDeBargis } from "./ChateauneufValDeBargis/ChateauneufValDeBargis"

export const nievreCities: City[] = [
  chateauneufValDeBargis
]
