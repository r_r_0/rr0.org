import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { JuraCityCode } from "../JuraCityCode"

export const saintClaude = franceCity(JuraCityCode.SaintClaude, Place.fromDMS("47°23′55″N,4°32′33″E"))
