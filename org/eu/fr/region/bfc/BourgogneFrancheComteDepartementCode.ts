import { FranceDepartementCode } from "../FranceDepartementCode"

export enum BourgogneFrancheComteDepartementCode {
  CoteDOr = FranceDepartementCode.CoteDOr,
  Doubs = FranceDepartementCode.Doubs,
  Jura = FranceDepartementCode.Jura,
  Morbihan = FranceDepartementCode.Morbihan,
  Nievre = FranceDepartementCode.Nievre,
  SaoneEtLoire = FranceDepartementCode.SaoneEtLoire,
  Yonne = FranceDepartementCode.Yonne
}
