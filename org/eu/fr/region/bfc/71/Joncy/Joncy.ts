import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SaoneEtLoireCityCode } from "../SaoneEtLoireCityCode"

export const joncy = franceCity(SaoneEtLoireCityCode.Joncy, Place.fromDMS("46° 36′ 51″N, 4° 33′ 32″E"))
