import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { SaoneEtLoireCityCode } from "../SaoneEtLoireCityCode"

export const verosvres = franceCity(SaoneEtLoireCityCode.Verosvres, Place.fromDMS("46°24′05″N,4°26′38″E"))
