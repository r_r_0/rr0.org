import { City } from "../../../../../country/region/department/city/City"
import { verosvres } from "./Verosvres/Verosvres"
import { joncy } from "./Joncy/Joncy"

export const saoneEtLoireCities: City[] = [
  joncy,
  verosvres
]
