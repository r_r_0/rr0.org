import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YonneCityCode } from "../YonneCityCode"

export const flognyLaChapelle = franceCity(YonneCityCode.FlognyLaChapelle, Place.fromDMS("47°57′11″N,3°52′23″E"))
