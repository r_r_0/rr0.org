import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YonneCityCode } from "../YonneCityCode"

export const precyLeSec = franceCity(YonneCityCode.PrecyLeSec, Place.fromDMS("47°52′07″N,3°45′06″E"))
