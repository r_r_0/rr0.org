import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YonneCityCode } from "../YonneCityCode"

export const jouxLaVille = franceCity(YonneCityCode.JouxLaVille, Place.fromDMS("47° 37′ 24″N,3°51′47″E"))
