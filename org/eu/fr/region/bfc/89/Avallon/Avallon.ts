import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YonneCityCode } from "../YonneCityCode"

export const avallon = franceCity(YonneCityCode.Avallon, Place.fromDMS("47° 29′ 27″ N, 3° 54′ 33″E"))
