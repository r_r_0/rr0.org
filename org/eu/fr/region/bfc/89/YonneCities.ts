import { City } from "../../../../../country/region/department/city/City"
import { provency } from "./Provency/Provency"
import { avallon } from "./Avallon/Avallon"
import { flognyLaChapelle } from "./FlognyLaChapelle/FlognyLaChapelle"
import { villy89 } from "./Villy/Villy"
import { saintMore89 } from "./SaintMore/SaintMore"
import { vermenton89 } from "./Vermenton/Vermenton"
import { precyLeSec } from "./PrecyLeSec/PrecyLeSec"
import { jouxLaVille } from "./JouxLaVille/JouxLaVille"

export const yonneCities: City[] = [
  avallon,
  flognyLaChapelle,
  jouxLaVille,
  precyLeSec,
  provency,
  saintMore89,
  vermenton89,
  villy89
]
