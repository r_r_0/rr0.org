import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { YonneCityCode } from "../YonneCityCode"

export const provency = franceCity(YonneCityCode.Provency, Place.fromDMS("47° 32′ 50″N, 3° 57′ 23″E"))
