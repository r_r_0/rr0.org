import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { MorbihanCityCode } from "../MorbihanCityCode"

export const locmine = franceCity(MorbihanCityCode.Locmine, Place.fromDMS(`47° 53′ 15″N, 2° 50′ 04″W`))
