import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { DoubsCityCode } from "../DoubsCityCode"

export const amathayVesigneux = franceCity(DoubsCityCode.AmathayVesigneux, Place.fromDMS("47° 01′ 28″N,6°12′03″E"))
