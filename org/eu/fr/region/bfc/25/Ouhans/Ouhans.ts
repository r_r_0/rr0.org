import { franceCity } from "../../../FranceCity"
import { Place } from "../../../../../../../place/Place"
import { DoubsCityCode } from "../DoubsCityCode"

export const ouhans = franceCity(DoubsCityCode.Ouhans, Place.fromDMS("46° 59′ 57″ N, 6° 17′ 39″E"))
