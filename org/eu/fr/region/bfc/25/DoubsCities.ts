import { City } from "../../../../../country/region/department/city/City"
import { ouhans } from "./Ouhans/Ouhans"
import { amathayVesigneux } from "./AmathayVesigneux/AmathayVesigneux"

export const doubsCities: City[] = [
  amathayVesigneux,
  ouhans
]
