import { Department } from "../../country/region/department/Department"
import { englandDepartments } from "./eng/EnglandDepartments"

export const ukDepartments: Department[] = [
  ...englandDepartments
]
