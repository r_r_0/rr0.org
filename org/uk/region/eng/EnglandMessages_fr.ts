import { RegionMessages } from "../../../country/region/RegionMessages"
import { englandDepartmentsMessages } from "./EnglandMessages"

export const englandMessages_fr = RegionMessages.create("Angleterre", englandDepartmentsMessages)
