import { Department } from "../../../country/region/department/Department"
import { sussex } from "./sussex/Sussex"

export const englandDepartments: Department[] = [
  sussex
]
