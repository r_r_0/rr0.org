import { UkDepartementCode } from "../UkDepartementCode"

export enum EnglandDepartementCode {
  Sussex = UkDepartementCode.Sussex
}
