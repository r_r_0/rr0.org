import { UkRegionCode } from "../UkRegionCode"
import { Place } from "../../../../place/Place"
import { ukRegion } from "../../Uk"

export const england = ukRegion(UkRegionCode.eng, Place.fromDMS("53°0'N,1°0'W"))
