import { UkDepartementCode } from "../../UkDepartementCode"
import { england } from "../England"
import { Place } from "../../../../../place/Place"
import { ukDepartment } from "../../UkDepartment"

export const sussex = ukDepartment(UkDepartementCode.Sussex, england, Place.fromDMS("47°20′N,1°40′O"))
