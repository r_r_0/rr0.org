import { RegionMessages } from "../../../country/region/RegionMessages"
import { englandDepartmentsMessages } from "./EnglandMessages"

export const englandMessages_en = RegionMessages.create("England", englandDepartmentsMessages)
