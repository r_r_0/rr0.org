import { City } from "../../../country/region/department/city/City"
import { sussexCities } from "./sussex/SussexCities"

export const englandCities: City[] = [
  ...sussexCities
]
