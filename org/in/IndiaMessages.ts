import { RegionMessages } from "../country/region/RegionMessages"
import { IndiaRegionCode } from "./IndiaRegionCode"

export type IndiaMessages = { [key in IndiaRegionCode]: RegionMessages }
