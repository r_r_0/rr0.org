import { indiaRegion } from "../IndiaRegion"
import { IndiaRegionCode } from "../IndiaRegionCode"
import { Place } from "../../../../place/Place"

export let maharashtra = indiaRegion(IndiaRegionCode.wa, Place.fromLocation(47.466667, 0.833333))
