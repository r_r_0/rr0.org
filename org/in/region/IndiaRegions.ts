import { telangana } from "./tg/Telangana"
import { City } from "../../country/region/department/city/City"
import { maharashtra } from "./mh/Maharashtra"

export const indiaRegions: City[] = [
  maharashtra,
  telangana
]
