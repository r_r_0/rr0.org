import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const india = new Country(CountryCode.in)
