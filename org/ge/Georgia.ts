import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const georgia = new Country(CountryCode.ge)
