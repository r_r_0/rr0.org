import { OrganizationMessages } from "../../../../OrganizationMessages"

export interface CityMessagesList {
  [code: string]: OrganizationMessages
}
