import { NewZealandRegionCode } from "./region/NewZealandRegionCode"
import { RegionMessages } from "../country/region/RegionMessages"

export type NewZealandMessages = { [key in NewZealandRegionCode]: RegionMessages }
