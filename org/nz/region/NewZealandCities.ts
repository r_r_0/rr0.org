import { Organization } from "../../Organization"
import { gisborneCities } from "./gisborne/GisborneCities"

export const newZealandCities: Organization[] = [
  ...gisborneCities
]
