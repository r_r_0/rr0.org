import { gisborneCity } from "./gisborne/Gisborne"
import { City } from "../../../country/region/department/city/City"

export const gisborneCities: City[] = [
  gisborneCity
]
