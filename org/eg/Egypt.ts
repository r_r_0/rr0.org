import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const egypt = new Country(CountryCode.eg)
