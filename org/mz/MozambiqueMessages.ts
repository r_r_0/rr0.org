import { RegionMessages } from "../country/region/RegionMessages"
import { MozambiqueRegionCode } from "./region/MozambiqueRegionCode"

export type MozambiqueRegionMessagesList = { [key in MozambiqueRegionCode]: RegionMessages }
