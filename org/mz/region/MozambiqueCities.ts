import { City } from "../../country/region/department/city/City"
import { sofalaCities } from "./sofala/SofalaCities"

export const mozambiqueCities: City[] = [
  ...sofalaCities
]
