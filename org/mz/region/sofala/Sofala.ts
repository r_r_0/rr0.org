import { MozambiqueRegionCode } from "../MozambiqueRegionCode"
import { Place } from "../../../../place/Place"
import { mozambiqueRegion } from "../../Mozambique"

export const sofala = mozambiqueRegion(MozambiqueRegionCode.sofala, Place.fromDMS("19°00′S,34°45′E"))
