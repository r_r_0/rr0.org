import { CityMessages } from "../../../country/region/department/city/CityMessages"

export type SofalaDepartmentCityList = { [key in SofalaCityCode]: CityMessages<any> }
