import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const algeria = new Country(CountryCode.dz)
