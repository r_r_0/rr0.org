import { CountryMessages } from "../country/CountryMessages"
import { AlgeriaRegionCode } from "./region/AlgeriaRegionCode"
import { algerRegion_fr } from "./region/al/AlgerRegion_fr"

export const algeria_fr = CountryMessages.create("Canada", {
    [AlgeriaRegionCode.al]: algerRegion_fr
  }
)
