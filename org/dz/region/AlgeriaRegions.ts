import { Region } from "../../country/region/Region"
import { algerRegion } from "./al/AlgerRegion"

export const algeriaRegions: Region[] = [
  algerRegion
]
