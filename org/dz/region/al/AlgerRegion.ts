import { algeriaRegion } from "../AlgeriaRegion"
import { AlgeriaRegionCode } from "../AlgeriaRegionCode"
import { Place } from "../../../../place/Place"

export let algerRegion = algeriaRegion(AlgeriaRegionCode.al, Place.fromDMS("36°42′N,3°13′E"))
