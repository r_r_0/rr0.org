import { CountryMessages } from "../country/CountryMessages"
import { SouthKoreaRegionMessagesList } from "./DominicanRepublicMessages"

export const dominicanRepublic_en = new CountryMessages<SouthKoreaRegionMessagesList>(["Dominican Republic"], {})
