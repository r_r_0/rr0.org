import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const panama = new Country(CountryCode.pa)
