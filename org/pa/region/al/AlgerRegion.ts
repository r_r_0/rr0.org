import { panamaRegion } from "../PanamaRegion"
import { PanamaRegionCode } from "../PanamaRegionCode"
import { Place } from "../../../../place/Place"

export let algerRegion = panamaRegion(PanamaRegionCode.al, Place.fromDMS("36°42′N,3°13′E"))
