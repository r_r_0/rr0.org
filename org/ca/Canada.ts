import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const canada = new Country(CountryCode.ca)
