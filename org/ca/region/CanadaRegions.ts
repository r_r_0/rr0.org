import { britishColumbia } from "./bc/BritishColumbia"
import { Region } from "../../country/region/Region"
import { manitoba } from "./mb/Manitoba"
import { quebec } from "./qc/Quebec"
import { alberta } from "./ab/Alberta"

export const canadaRegions: Region[] = [
  alberta,
  manitoba,
  quebec,
  britishColumbia
]
