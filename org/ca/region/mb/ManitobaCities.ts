import { City } from "../../../country/region/department/city/City"
import { eastmanCities } from "./eastman/EastmanCities"

export const manitobaCities: City[] = [
  ...eastmanCities
]
