import { kootenaysCityMessages } from "./KootenaysCityMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export const kootenaysMessages_fr = DepartmentMessages.create("Kootenay", kootenaysCityMessages)
