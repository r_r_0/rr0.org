import { castlegarMessages } from "./Castlegar/CastegarMessages"
import { CityMessagesList } from "../../../../country/region/department/city/CityMessagesList"
import { KootenaysCityCode } from "./KootenaysCityCode"

export const kootenaysCityMessages: CityMessagesList = {
  [KootenaysCityCode.Castegar]: castlegarMessages
}
