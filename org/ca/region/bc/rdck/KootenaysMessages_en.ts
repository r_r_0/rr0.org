import { kootenaysCityMessages } from "./KootenaysCityMessages"
import { DepartmentMessages } from "../../../../country/region/department/DepartmentMessages"

export const kootenaysMessages_en = DepartmentMessages.create("Kootenays", kootenaysCityMessages)
