import { City } from "../../../country/region/department/city/City"
import { kootenaysCities } from "./rdck/KootenaysCities"

export const britishColumbiaCities: City[] = [
  ...kootenaysCities
]
