import { CountryMessages } from "../country/CountryMessages"
import { TaiwanRegionMessagesList } from "./TaiwanMessages"

export const taiwan_en = CountryMessages.create<TaiwanRegionMessagesList>("Taiwan", {})
