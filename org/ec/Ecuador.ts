import { Country } from "../country/Country"
import { CountryCode } from "../country/CountryCode"

export const ecuador = new Country(CountryCode.ec)
