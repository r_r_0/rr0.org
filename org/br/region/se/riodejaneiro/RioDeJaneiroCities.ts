import { rioDeJaneiroCity } from "./20000-000/RioDeJaneiro"
import { City } from "../../../../country/region/department/city/City"

export const rioDeJaneiroCities: City[] = [
  rioDeJaneiroCity
]
