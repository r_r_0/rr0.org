import { rioDeJaneiro } from "./riodejaneiro/RioDeJaneiro"
import { Organization } from "../../../Organization"

export const southEastDepartments: Organization[] = [
  rioDeJaneiro
]
