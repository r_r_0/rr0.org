import { rioDeJaneiroCities } from "./riodejaneiro/RioDeJaneiroCities"
import { Organization } from "../../../Organization"

export const southEastCities: Organization[] = [
  ...rioDeJaneiroCities
]
