import { brasilia } from "./70000-000/Brasilia"
import { Organization } from "../../../../Organization"

export const federalDistrictCities: Organization[] = [
  brasilia
]
