import { Department } from "../../../country/region/department/Department"
import { federalDistrict } from "./federaldistrict/FederalDistrict"

export const centralWestDepartments: Department[] = [
  federalDistrict
]
