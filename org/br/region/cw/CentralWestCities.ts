import { federalDistrictCities } from "./federaldistrict/FederalDistrictCities"
import { Organization } from "../../../Organization"

export const centralWestCities: Organization[] = [
  ...federalDistrictCities
]
