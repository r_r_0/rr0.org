import { centralWest } from "./cw/CentralWest"
import { Organization } from "../../Organization"
import { southEast } from "./se/SouthEast"

export const brazilRegions: Organization[] = [
  centralWest,
  southEast
]
