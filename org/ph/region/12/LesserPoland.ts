import { Place } from "../../../../../place/Place"
import { PhilippinesRegionCode } from "../PhilippinesRegionCode"
import { philippinesRegion } from "../PhilippinesRegion"

export const lesserPoland = philippinesRegion(PhilippinesRegionCode.lesserPoland, Place.fromDMS("50°3′41″N,19°56′18″E"))
