import {RR0Messages_fr} from "./RR0Messages_fr"
import {RR0Messages_en} from "./RR0Messages_en"
import {RR0Messages} from "./RR0Messages"

export const ssgMessages: { [lang: string]: RR0Messages } = {
  "fr": new RR0Messages_fr(),
  "en": new RR0Messages_en()
}
